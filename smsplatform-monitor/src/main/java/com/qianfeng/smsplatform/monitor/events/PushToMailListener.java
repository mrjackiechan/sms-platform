package com.qianfeng.smsplatform.monitor.events;


//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼            BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  


import com.sun.mail.util.MailSSLSocketFactory;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * Created by jackiechan on 2021/7/26 17:46
 *
 * @author jackiechan
 * 人学始知道，不学非自然。
 */
@Component
public class PushToMailListener {

    @EventListener
    @Async//异步执行事件
    public void onEvent(QueueLimitEvent event) {
        //此处listener 的作用是发送邮件给指定的人
        Long channelId = event.getQueueId();
        Long currentCount = event.getCurrentCount();
        Properties properties = new Properties();
        properties.put("mail.host","smtp.qq.com");
        properties.put("mail.transport.protocol","smtp");
        properties.put("mail.smtp.auth","true");

        try {
            MailSSLSocketFactory sf = new MailSSLSocketFactory();
            sf.setTrustAllHosts(true);//信任所有主机,也就是说会信任我们的目标邮箱服务器给我们的证书,
            properties.put("mail.smtp.ssl.enable", "true");//开启 ssl 的支持
            properties.put("mail.smtp.ssl.socketFactory", sf);//设置 ssl 的连接工厂
            //创建会话,并设置要会话的服务器相关信息
            Session session = Session.getDefaultInstance(properties, new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication("5433708@qq.com", "dlkultrmntwkbjdd");
                }
            });
            session.setDebug(true);//在控制台显示发送的具体详情

            //像连接数据库一样,我们会话(连接)建立好了,需要继续进行下一步操作,也就是类似 statement
            Transport transport = session.getTransport();
            //正式连接
            transport.connect("smtp.qq.com", "5433708@qq.com", "dlkultrmntwkbjdd");

            //发送邮件
            MimeMessage message = new MimeMessage(session);

            message.setFrom(new InternetAddress("5433708@qq.com"));
            InternetAddress internetAddress1 = new InternetAddress("1162067183@qq.com");
            //InternetAddress wuqi = new InternetAddress("wuqi114477@163.com");//wuqi 要 si qu
            // InternetAddress[] addresses = new InternetAddress[]{internetAddress1,wuqi};
            InternetAddress[] addresses = new InternetAddress[]{internetAddress1};
            message.setRecipients(Message.RecipientType.TO, addresses);
            //设置标题
            message.setSubject("拿来吧你!!");

            //设置内容
            message.setContent("消息队列" + channelId + "超出预警值了当前数量是:==>"+currentCount, "text/html;charset=utf-8");
            //发送
            transport.sendMessage(message, message.getAllRecipients());
            //关闭连接
            transport.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

package com.qianfeng.smsplatform.monitor.tasks;


//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼            BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  


import com.qianfeng.smsplatform.common.constants.RabbitMqConsants;
import com.qianfeng.smsplatform.monitor.events.QueueLimitEvent;
import com.qianfeng.smsplatform.monitor.service.QueueService;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import org.apache.shardingsphere.elasticjob.api.ShardingContext;
import org.apache.shardingsphere.elasticjob.simple.job.SimpleJob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

/**
 * Created by jackiechan on 2021/7/26 16:26
 *
 * @author jackiechan
 * 人学始知道，不学非自然。
 */
@Component
public class MonitorQueueTask implements SimpleJob {

    private Channel channel;


    private ApplicationContext context;

    @Autowired
    public void setContext(ApplicationContext context) {
        this.context = context;
    }

    @Autowired
    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    private QueueService queueService;

    @Autowired
    public void setQueueService(QueueService queueService) {
        this.queueService = queueService;
    }

    @Override
    public void execute(ShardingContext shardingContext) {
//        System.err.println(shardingContext.getShardingItem() + "----->" + shardingContext.getShardingParameter());//leige,yuhaoran这种具体的标识
//        switch (shardingContext.getShardingItem()) {
//            case 0:
//                System.err.println("买菜");
//                break;
//            case 1:
//                System.err.println("买酒");
//                break;
//            case 2:
//                System.err.println("买肉");
//                break;
//            case 3:
//                System.err.println("干饭");
//                break;
//            case 4:
//                System.err.println("红伞伞白杆杆");
//                break;
//
//        }

        //我们需要监控的是网关的队列,需要知道网关队列的名字,但是网关的队列名字是变化的,根据不同的通道会有不同的网关 id,就会有不同的队列名字
        //所以我们需要找到所有的网关的 id,然后拼接出每一个网关的队列的名字
        //然后 for 循环,挨个获取数据,获取到数据之后进行判断,如果超出阈值,则告警
        List<Long> ids = queueService.findAllQueueId(); //获取到所有的网关的 id
        for (Long id : ids) {
            String queuename = RabbitMqConsants.TOPIC_SMS_GATEWAY + id;
            try {
                AMQP.Queue.DeclareOk declarePassive = channel.queueDeclarePassive(queuename);
                int messageCount = declarePassive.getMessageCount();
                System.err.println(queuename + "获取到的消息的数量是:" + messageCount);
                if (messageCount > 50) {
                    System.err.println(queuename + ":超出了限制的阈值,需要告警");
                    context.publishEvent(new QueueLimitEvent(id, (long) messageCount));
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }
}

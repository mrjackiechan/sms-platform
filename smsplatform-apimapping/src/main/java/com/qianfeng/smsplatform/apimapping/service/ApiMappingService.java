package com.qianfeng.smsplatform.apimapping.service;

import com.github.pagehelper.PageInfo;
import com.qianfeng.smsplatform.apimapping.pojo.TApiMapping;

import java.util.List;
import java.util.Map;

public interface ApiMappingService {
    void addApiMapping(TApiMapping mapping);

    void updateApiMapping(TApiMapping mapping);

    PageInfo getMappingList(Map criteria, int offset, int limit);

    TApiMapping getMappingById(Integer id);

    void deleteMapping(Integer[] ids);

    List<TApiMapping> findAll();

    List<TApiMapping> findAll(Byte state);
}

package com.qianfeng.smsplatform.apimapping.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qianfeng.smsplatform.apimapping.mapper.TApiMappingMapper;
import com.qianfeng.smsplatform.apimapping.pojo.TApiMapping;
import com.qianfeng.smsplatform.apimapping.pojo.TApiMappingExample;
import com.qianfeng.smsplatform.apimapping.service.ApiMappingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class ApiMappingServiceImpl implements ApiMappingService {

    private TApiMappingMapper apiMappingMapper;

    @Autowired
    public void setApiMappingMapper(TApiMappingMapper apiMappingMapper) {
        this.apiMappingMapper = apiMappingMapper;
    }


    @Override
    public void addApiMapping(TApiMapping mapping) {
        mapping.setCreateDate(new Date());
        apiMappingMapper.insert(mapping);
    }

    @Override
    public void updateApiMapping(TApiMapping mapping) {
        apiMappingMapper.updateByPrimaryKeySelective(mapping);
    }

    @Override
    public PageInfo getMappingList(Map criteria, int offset, int limit) {
        PageHelper.offsetPage(offset, limit);
        List<TApiMapping> apiMappingList = apiMappingMapper.getMappingList(criteria);
        PageInfo<TApiMapping> info = new PageInfo<>(apiMappingList);
        return info;
    }

    @Override
    public TApiMapping getMappingById(Integer id) {
        return apiMappingMapper.selectByPrimaryKey(id);
    }

    @Override
    public void deleteMapping(Integer[] ids) {
        if (ids == null || ids.length == 0) {
            return;
        }
        TApiMapping tApiMapping = new TApiMapping();
        tApiMapping.setState((byte) 0);
        TApiMappingExample example = new TApiMappingExample();
        example.createCriteria().andIdIn(Arrays.asList(ids));
        apiMappingMapper.updateByExampleSelective(tApiMapping, example);
    }

    @Override
    public List<TApiMapping> findAll() {
        TApiMappingExample example = new TApiMappingExample();
        return apiMappingMapper.selectByExample(example);
    }

    @Override
    public List<TApiMapping> findAll(Byte state) {
        TApiMappingExample example = new TApiMappingExample();
        example.createCriteria().andStateEqualTo(state);
        return apiMappingMapper.selectByExample(example);
    }
}

package com.qianfeng.smsplatform.apimapping.mapper;

import com.qianfeng.smsplatform.apimapping.pojo.TApiMapping;
import com.qianfeng.smsplatform.apimapping.pojo.TApiMappingExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface TApiMappingMapper {
    long countByExample(TApiMappingExample example);

    int deleteByExample(TApiMappingExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(TApiMapping record);

    int insertSelective(TApiMapping record);

    List<TApiMapping> selectByExample(TApiMappingExample example);

    TApiMapping selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") TApiMapping record, @Param("example") TApiMappingExample example);

    int updateByExample(@Param("record") TApiMapping record, @Param("example") TApiMappingExample example);

    int updateByPrimaryKeySelective(TApiMapping record);

    int updateByPrimaryKey(TApiMapping record);
    List<TApiMapping> getMappingList(Map criteria);
}
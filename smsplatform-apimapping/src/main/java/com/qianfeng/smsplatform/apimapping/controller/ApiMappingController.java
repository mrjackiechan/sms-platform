package com.qianfeng.smsplatform.apimapping.controller;

import com.github.pagehelper.PageInfo;
import com.qianfeng.smsplatform.apimapping.pojo.TApiMapping;
import com.qianfeng.smsplatform.apimapping.service.ApiMappingService;
import com.qianfeng.smsplatform.common.dto.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 路由管理
 */
@RestController
@RequestMapping("/apimapping")
public class ApiMappingController {

    @Autowired
    private ApiMappingService apiMappingService;

    @RequestMapping("/list")
    public PageInfo table(@RequestBody Map params, @RequestParam(defaultValue = "1") Integer offset,@RequestParam(defaultValue = "10") Integer limit) {
        PageInfo pageInfo = apiMappingService.getMappingList(params, offset, limit);
        return pageInfo;
    }

    @RequestMapping("/save")
    public R add(@RequestBody TApiMapping tApiMapping) {
        apiMappingService.addApiMapping(tApiMapping);
        return R.ok();
    }

    @RequestMapping("/update")
    public R update(@RequestBody TApiMapping tApiMapping) {
        apiMappingService.updateApiMapping(tApiMapping);
        return R.ok();
    }

    @RequestMapping("/info/{id}")
    public TApiMapping info(@PathVariable Integer id) {
        TApiMapping mapping = apiMappingService.getMappingById(id);
      //  return R.ok().put("apimapping", mapping);
        return mapping;
    }

    @RequestMapping("/del")
    public R delete(@RequestBody Integer[] ids) {
        apiMappingService.deleteMapping(ids);
        return R.ok();
    }

    @RequestMapping("/all")
    public List<TApiMapping> findAll() {
        return  apiMappingService.findAll();
    }

    @RequestMapping("/allbystate")
    public List<TApiMapping> findAll(@RequestParam(defaultValue = "1") Byte state) {
        return  apiMappingService.findAll(state);
    }
}

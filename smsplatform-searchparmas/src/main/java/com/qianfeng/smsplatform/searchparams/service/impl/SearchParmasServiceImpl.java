package com.qianfeng.smsplatform.searchparams.service.impl;

//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼                  BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  
//  


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qianfeng.smsplatform.common.check.CheckType;
import com.qianfeng.smsplatform.common.dto.QueryDTO;
import com.qianfeng.smsplatform.common.exceptions.DeleteDataErrorException;
import com.qianfeng.smsplatform.common.exceptions.QueryDataErrorException;
import com.qianfeng.smsplatform.common.exceptions.UpdateDataErrorException;
import com.qianfeng.smsplatform.searchparams.mapper.TSearchParmasMapper;
import com.qianfeng.smsplatform.searchparams.pojo.TSearchParmas;
import com.qianfeng.smsplatform.searchparams.pojo.TSearchParmasExample;
import com.qianfeng.smsplatform.searchparams.service.SearchParmasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * Created by Administrator on 2021/8/9/18:51
 *
 * @author Administrator
 * @version 1.0
 * @since 1.0
 */
@Service
@Transactional
public class SearchParmasServiceImpl implements SearchParmasService {


    private TSearchParmasMapper searchParmasMapper;

    @Autowired
    public void setSearchParmasMapper(TSearchParmasMapper searchParmasMapper) {
        this.searchParmasMapper = searchParmasMapper;
    }

    @Override
    public int updateSearchParmas(TSearchParmas searchParmas) {
        if (searchParmas.isNull(CheckType.UPDATE)) {
            throw new UpdateDataErrorException("没有传递主键", 500);
        }
        int update = searchParmasMapper.updateByPrimaryKeySelective(searchParmas);//更新数据
        return update;
    }

    @Override
    public void addSearchParmas(TSearchParmas searchParmas) {
        if (searchParmas.isNull(CheckType.ADD)) {
            throw new UpdateDataErrorException("数据不完整", 500);
        }
        int insert = searchParmasMapper.insert(searchParmas);

    }

    @Override
    public int deleteSearchParmas(List<Integer> ids) {
        if (ids == null || ids.size() == 0) {
            throw new DeleteDataErrorException("没有传递id", 500);
        }
        TSearchParmasExample example = new TSearchParmasExample();
        example.createCriteria().andIdIn(ids);
        int delete = searchParmasMapper.deleteByExample(example);
        //发送事件更新数据
        return delete;
    }

    @Override
    public TSearchParmas findById(Integer id) {
        if (id == null || id <= 0) {
            throw new QueryDataErrorException("没有传递id", 500);
        }
        TSearchParmas searchParmas = searchParmasMapper.selectByPrimaryKey(id);
        return searchParmas;
    }

    @Override
    public List<TSearchParmas> findAll() {
        TSearchParmasExample example = new TSearchParmasExample();
        List<TSearchParmas> parmasList = searchParmasMapper.selectByExample(example);
        return parmasList;
    }

    @Override
    public PageInfo findByPage(QueryDTO queryDTO) {
        PageHelper.offsetPage(queryDTO.getOffset(), queryDTO.getLimit());
        TSearchParmasExample example = new TSearchParmasExample();
        String sort = queryDTO.getSort();
        if (!StringUtils.isEmpty(sort)) {
            example.setOrderByClause("id");
        }
        List<TSearchParmas> tSearchParmasList = searchParmasMapper.selectByExample(example);
        PageInfo<TSearchParmas> info = new PageInfo<>(tSearchParmasList);
        return info;
    }

    @Override
    public List<TSearchParmas> findByState(Short state) {
        TSearchParmasExample example = new TSearchParmasExample();
        example.createCriteria().andStateEqualTo(state);
        List<TSearchParmas> tSearchParmasList = searchParmasMapper.selectByExample(example);
        return tSearchParmasList;
    }

}

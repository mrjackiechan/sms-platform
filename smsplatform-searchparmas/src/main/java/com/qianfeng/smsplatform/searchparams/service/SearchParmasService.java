package com.qianfeng.smsplatform.searchparams.service;

import com.github.pagehelper.PageInfo;
import com.qianfeng.smsplatform.common.dto.QueryDTO;
import com.qianfeng.smsplatform.searchparams.pojo.TSearchParmas;

import java.util.List;

public interface SearchParmasService {

    int updateSearchParmas(TSearchParmas searchParmas);


    void addSearchParmas(TSearchParmas searchParmas);

    int deleteSearchParmas(List<Integer> ids);

    TSearchParmas findById(Integer id);

    List<TSearchParmas> findAll();

    PageInfo findByPage(QueryDTO queryDTO);

    List<TSearchParmas> findByState(Short state);

}
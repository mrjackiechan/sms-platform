package com.qianfeng.smsplatform.grayrelease.dao;

import com.qianfeng.smsplatform.grayrelease.pojo.TGrayReleaseConfig;
import com.qianfeng.smsplatform.grayrelease.pojo.TGrayReleaseConfigExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TGrayReleaseConfigMapper {
    long countByExample(TGrayReleaseConfigExample example);

    int deleteByExample(TGrayReleaseConfigExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(TGrayReleaseConfig record);

    int insertSelective(TGrayReleaseConfig record);

    List<TGrayReleaseConfig> selectByExample(TGrayReleaseConfigExample example);

    TGrayReleaseConfig selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") TGrayReleaseConfig record, @Param("example") TGrayReleaseConfigExample example);

    int updateByExample(@Param("record") TGrayReleaseConfig record, @Param("example") TGrayReleaseConfigExample example);

    int updateByPrimaryKeySelective(TGrayReleaseConfig record);

    int updateByPrimaryKey(TGrayReleaseConfig record);
}
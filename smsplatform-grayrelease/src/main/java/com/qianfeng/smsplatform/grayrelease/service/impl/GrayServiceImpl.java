package com.qianfeng.smsplatform.grayrelease.service.impl;

//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼                  BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  
//  


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qianfeng.smsplatform.common.check.CheckType;
import com.qianfeng.smsplatform.common.dto.QueryDTO;
import com.qianfeng.smsplatform.common.exceptions.AddDataErrorException;
import com.qianfeng.smsplatform.common.exceptions.DeleteDataErrorException;
import com.qianfeng.smsplatform.common.exceptions.QueryDataErrorException;
import com.qianfeng.smsplatform.grayrelease.dao.TGrayReleaseConfigMapper;
import com.qianfeng.smsplatform.grayrelease.pojo.TGrayReleaseConfig;
import com.qianfeng.smsplatform.grayrelease.pojo.TGrayReleaseConfigExample;
import com.qianfeng.smsplatform.grayrelease.service.GrayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * Created by Jackiechan on 2021/11/21/下午11:05
 *
 * @author Jackiechan
 * @version 1.0
 * 人学始知道，不学非自然。
 * @since 1.0
 */
@Service
@Transactional
public class GrayServiceImpl implements GrayService {


    private TGrayReleaseConfigMapper tGrayReleaseConfigMapper;

    @Autowired
    public void settGrayReleaseConfigMapper(TGrayReleaseConfigMapper tGrayReleaseConfigMapper) {
        this.tGrayReleaseConfigMapper = tGrayReleaseConfigMapper;
    }


    @Override
    public int addGrayReleaseConfig(TGrayReleaseConfig TGrayReleaseConfig) {
        if (TGrayReleaseConfig.isNull(CheckType.ADD)) {
            throw new AddDataErrorException("数据不完整", 500);
        }
        return tGrayReleaseConfigMapper.insert(TGrayReleaseConfig);
    }

    @Override
    public int delGrayReleaseConfigs(List<Integer> ids) {
        if (ids == null || ids.size() == 0) {
            throw new DeleteDataErrorException("没有传递id", 500);
        }
        TGrayReleaseConfigExample example = new TGrayReleaseConfigExample();
        example.createCriteria().andIdIn(ids);
        int result = tGrayReleaseConfigMapper.deleteByExample(example);
        return result;
    }

    @Override
    public int updateGrayReleaseConfig(TGrayReleaseConfig tGrayReleaseConfig) {
        if (tGrayReleaseConfig.isNull(CheckType.UPDATE)) {
            throw new AddDataErrorException("数据不完整", 500);
        }
        return tGrayReleaseConfigMapper.updateByPrimaryKeySelective(tGrayReleaseConfig);
    }

    @Override
    public TGrayReleaseConfig findById(Integer id) {
        if (id == null || id <= 0) {
            throw new QueryDataErrorException("没有传递主键", 500);
        }
        return tGrayReleaseConfigMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<TGrayReleaseConfig> findAll() {
        TGrayReleaseConfigExample example = new TGrayReleaseConfigExample();
        return tGrayReleaseConfigMapper.selectByExample(example);
    }

    @Override
    public PageInfo findByPage(QueryDTO queryDTO) {
        PageHelper.offsetPage(queryDTO.getOffset(), queryDTO.getLimit());
        TGrayReleaseConfigExample example = new TGrayReleaseConfigExample();
        String sort = queryDTO.getSort();
        if (!StringUtils.isEmpty(sort)) {
            example.setOrderByClause("id");
        }
        List<TGrayReleaseConfig> grayReleaseConfigList = tGrayReleaseConfigMapper.selectByExample(example);
        PageInfo<TGrayReleaseConfig> info = new PageInfo<>(grayReleaseConfigList);
        return info;
    }

    @Override
    public List<TGrayReleaseConfig> findByState(Short state) {
        TGrayReleaseConfigExample example = new TGrayReleaseConfigExample();
        example.createCriteria().andStateEqualTo(state);
        return tGrayReleaseConfigMapper.selectByExample(example);
    }
}

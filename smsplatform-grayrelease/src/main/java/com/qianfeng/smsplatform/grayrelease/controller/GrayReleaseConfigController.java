package com.qianfeng.smsplatform.grayrelease.controller;

//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼                  BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  
//  


import com.github.pagehelper.PageInfo;
import com.qianfeng.smsplatform.common.dto.QueryDTO;
import com.qianfeng.smsplatform.common.dto.R;
import com.qianfeng.smsplatform.grayrelease.pojo.TGrayReleaseConfig;
import com.qianfeng.smsplatform.grayrelease.service.GrayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Jackiechan on 2021/11/21/下午11:21
 *
 * @author Jackiechan
 * @version 1.0
 * 人学始知道，不学非自然。
 * @since 1.0
 */
@RestController
@RequestMapping("/grayreleaseconfig")
public class GrayReleaseConfigController {


    private GrayService grayService;
    @Autowired
    public void setGrayService(GrayService grayService) {
        this.grayService = grayService;
    }

    @PostMapping("/save")
    public R addGrayReleaseConfig(@RequestBody TGrayReleaseConfig tGrayReleaseConfig) {

        grayService.addGrayReleaseConfig(tGrayReleaseConfig);
        return R.ok();
    }

    @PostMapping("/del")
    public R delGrayReleaseConfigs(@RequestBody List<Integer> id) {
        grayService.delGrayReleaseConfigs(id);
        return R.ok();
    }

    @PostMapping("/update")
    public R updateLimit(@RequestBody TGrayReleaseConfig tGrayReleaseConfig) {
        grayService.updateGrayReleaseConfig(tGrayReleaseConfig);
        return R.ok();
    }


    @GetMapping("/info/{id}")
    public TGrayReleaseConfig findById(@PathVariable Integer id) {
        TGrayReleaseConfig releaseConfig = grayService.findById(id);
        return releaseConfig;
    }

    @GetMapping("/all")
    public  List<TGrayReleaseConfig> findAll() {
        List<TGrayReleaseConfig> grayReleaseConfigList = grayService.findAll();
        return grayReleaseConfigList;
    }

    @PostMapping("/list")
    public PageInfo findByPage(@RequestBody QueryDTO queryDTO) {
        PageInfo pageInfo = grayService.findByPage(queryDTO);
        return pageInfo;
    }

    @GetMapping("/state")
    List<TGrayReleaseConfig> findByState(@RequestParam(defaultValue = "1") short state) {
        return grayService.findByState(state);
    }

}

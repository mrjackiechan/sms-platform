package com.qianfeng.smsplatform.apigatefilters.controller;


//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼                  BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  


import com.github.pagehelper.PageInfo;
import com.qianfeng.smsplatform.common.dto.QueryDTO;
import com.qianfeng.smsplatform.common.dto.R;
import com.qianfeng.smsplatform.apigatefilters.bean.TFilter;
import com.qianfeng.smsplatform.apigatefilters.service.FiltersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by jackiechan on 2021-02-28 09:49
 *
 * @Author jackiechan
 */
@RestController
@RequestMapping("/apigatewayfilters")
public class FiltersController {
    @Autowired
    private FiltersService filtersService;

    /**
     * 修改某个分组的过滤器的顺序
     *
     * @return
     */
    @PostMapping("/update")
    public R updateFilters(@RequestBody TFilter filter) {
        int updateFilters = filtersService.updateFilters(filter);
        if (updateFilters == 1) {
            return R.ok();
        }
        return R.error();
    }


    /**
     * 添加一个过滤器的分组
     *
     * @param filter 过滤器
     * @return
     */
    @PostMapping("/save")
    public R addFilters(@RequestBody TFilter filter) {
        filtersService.addFilters(filter);
        return R.ok();

    }

    @PostMapping("/del")
    public R deleteFilters(@RequestBody List<Integer> ids) {
        filtersService.deleteFilters(ids);
        return R.ok();
    }

    @GetMapping("/info/{id}")
    public TFilter findById(@PathVariable Integer id) {
        TFilter filter = filtersService.findById(id);
        return filter;
    }

    @GetMapping("/all")
    public  List<TFilter>  findAll() {
        List<TFilter> filterList = filtersService.findAll();
        return filterList;
    }

    @PostMapping("/list")
    public PageInfo findByPage(@RequestBody QueryDTO queryDTO) {
        PageInfo pageInfo = filtersService.findByPage(queryDTO);
        return pageInfo;
    }
    @GetMapping("/state")
    List<TFilter> findByState(@RequestParam(defaultValue = "1") short state){
        return filtersService.findByState(state);
    }

}

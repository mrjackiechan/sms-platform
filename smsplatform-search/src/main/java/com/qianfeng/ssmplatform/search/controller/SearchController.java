package com.qianfeng.ssmplatform.search.controller;


//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼            BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qianfeng.ssmplatform.search.feign.CacheService;
import com.qianfeng.ssmplatform.search.service.DocService;
import com.qianfeng.ssmplatform.search.service.IndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jackiechan on 2021/7/22 10:03
 *
 * @author jackiechan
 * 人学始知道，不学非自然。
 */
@RestController
@RequestMapping("/search")
public class SearchController {

    private IndexService indexService;


    private DocService docService;

    private ObjectMapper objectMapper;

    @Autowired
    public void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    private CacheService cacheService;

    @Autowired
    public void setCacheService(CacheService cacheService) {
        this.cacheService = cacheService;
    }


    @Autowired
    public void setDocService(DocService docService) {
        this.docService = docService;
    }

    @Autowired
    public void setIndexService(IndexService indexService) {
        this.indexService = indexService;
    }

    @RequestMapping("/createindex")
    public String createIndex() throws IOException {
        indexService.createIndex();
        return "success";
    }

    @GetMapping("/testquery")
    public String testQuery() {

        try {
            docService.queryDataByCidAndPhoneAndSrcId(0,123456,"13963663635");
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "success";
    }

    @GetMapping("/search")
    public Map search(@RequestParam Map<String, String> params) {
        Map resultMap = new HashMap();
        try {
            List<Map> result = docService.queryDataByMultiCondition(params);
            String resultTotalCount = params.get("resultTotalCount");//当前条件查询到的总数据的长度
            resultMap.put("data", result);
            resultMap.put("count", resultTotalCount);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return resultMap;
    }




    @PostMapping("/params")
    public String addParams() {

        Map<String, Map<String, String>> params = new HashMap<>();

        Map<String, String> first = new HashMap<>();
        first.put("cloum", "messageContent");
        first.put("type", "match");
        params.put("content", first);

        Map<String, String> second = new HashMap<>();
        second.put("cloum", "destMobile");
        second.put("type", "term");
        params.put("mobile", second);


        Map<String, String> third = new HashMap<>();
        third.put("cloum", "sendTime");
        third.put("type", "range");
        third.put("order", "0");
        params.put("starttime", third);

        Map<String, String> fourth = new HashMap<>();
        fourth.put("cloum", "sendTime");
        fourth.put("type", "range");
        fourth.put("order", "1");
        params.put("stoptime", fourth);

        try {
            String json = objectMapper.writeValueAsString(params);
            System.err.println(json);
            cacheService.saveCache("searchparams",json);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return "aaa";
    }
}

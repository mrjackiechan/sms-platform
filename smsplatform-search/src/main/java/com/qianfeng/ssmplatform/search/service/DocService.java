package com.qianfeng.ssmplatform.search.service;
//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼            BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  


import com.qianfeng.smsplatform.common.model.Standard_Submit;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created by jackiechan on 2021/7/22 11:03
 * 主要负责文档的增删改查的
 * @author jackiechan
 * 人学始知道，不学非自然。
 */

public interface DocService {

    public void addDoc(Standard_Submit standard_submit) throws IOException;

    /**
     * 更新短信的状态
     * @param standard_submit
     */
    void updateDoc(Standard_Submit standard_submit) throws IOException;

    /**
     * 根据客户id和下行编号id以及手机号来查询数据
     * @param clientId
     * @param srcId
     * @param phoneNum
     * @return
     */
    Map queryDataByCidAndPhoneAndSrcId(int clientId, long srcId, String phoneNum) throws IOException;

    /**
     * 根据传递的条件进行筛选数据,条件用map的原因是因为数量会发生变化
     * @param params
     * @return
     */
    List<Map> queryDataByMultiCondition(Map<String,String> params) throws IOException;
}

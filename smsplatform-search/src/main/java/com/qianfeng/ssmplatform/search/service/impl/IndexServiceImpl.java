package com.qianfeng.ssmplatform.search.service.impl;


//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼            BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  


import com.qianfeng.ssmplatform.search.service.IndexService;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.json.JsonXContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * Created by jackiechan on 2021/7/22 10:39
 *
 * @author jackiechan
 * 人学始知道，不学非自然。
 */
@Service
public class IndexServiceImpl implements IndexService {
    @Value("${elasticsearch.index.name}")
    private String index;

    private RestHighLevelClient highLevelClient;

    @Autowired
    public void setHighLevelClient(RestHighLevelClient highLevelClient) {
        this.highLevelClient = highLevelClient;
    }

    @Override
    public void createIndex() throws IOException {

        CreateIndexRequest createIndexRequest = new CreateIndexRequest(index);
        //设置所谓分片
        createIndexRequest.settings(Settings.builder().put("number_of_shards",3).put("number_of_replicas",2).build());
        XContentBuilder xContentBuilder = JsonXContent.contentBuilder()
                .startObject()
                .startObject("properties")
                    .startObject("clientID").field("type","integer").field("index","true").endObject()
                    .startObject("srcNumber").field("type","keyword").field("index","true").endObject()
                    .startObject("messagePriority").field("type","short").field("index","true").endObject()
                    .startObject("srcSequenceId").field("type","long").field("index","true").endObject()
                    .startObject("gatewayID").field("type","integer").field("index","true").endObject()
                    .startObject("productID").field("type","integer").field("index","true").endObject()
                    .startObject("destMobile").field("type","keyword").field("index","true").endObject()
                    .startObject("messageContent").field("type","text").field("analyzer","ik_max_word").field("index","true").endObject()
                    .startObject("description").field("type","text").field("analyzer","ik_max_word").field("index","true").endObject()
                    .startObject("msgid").field("type","keyword").field("index","true").endObject()
                    .startObject("reportState").field("type","integer").field("index","true").endObject()
                    .startObject("errorCode").field("type","text").field("index","true").endObject()
                    .startObject("operatorId").field("type","integer").field("index","true").endObject()
                    .startObject("provinceId").field("type","integer").field("index","true").endObject()
                    .startObject("cityId").field("type","integer").field("index","true").endObject()
                    .startObject("source").field("type","integer").field("index","true").endObject()
                    .startObject("sendTime").field("type","date").field("format","yyyy-MM-dd HH:mm:ss").field("index","true").endObject()
                    .startObject("updateTime").field("type","date").field("format","yyyy-MM-dd HH:mm:ss").field("index","true").endObject()
                .endObject()
                .endObject();
        createIndexRequest.mapping(xContentBuilder);
        highLevelClient.indices().create(createIndexRequest, RequestOptions.DEFAULT);
    }
}

package com.qianfeng.ssmplatform.search.cache;


//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼            BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qianfeng.smsplatform.common.constants.CacheConstants;
import com.qianfeng.ssmplatform.search.events.SearchParamsChangeEvent;
import com.qianfeng.ssmplatform.search.feign.CacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jackiechan on 2021/7/23 09:47
 *
 * @author jackiechan
 * 人学始知道，不学非自然。
 */
@Component
@EnableAsync
public class ParamsLocalCache {

    private CacheService cacheService;
    private Map<String, Map<String, String>> paramsMap = new HashMap<>();


    public Map<String, Map<String, String>> getParamsMap() {
        return paramsMap;
    }

    private ObjectMapper objectMapper;

    @Autowired
    public void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;

    }

    @Autowired
    public void setCacheService(CacheService cacheService) {
        this.cacheService = cacheService;
    }

    @PostConstruct
    public void init() {
        String searchparamsString = cacheService.get(CacheConstants.CACHE_SEARCHPARAM_KEY);
        if (!StringUtils.isEmpty(searchparamsString)) {
            try {
                Map map = objectMapper.readValue(searchparamsString, Map.class);
                paramsMap.clear();
                paramsMap.putAll(map);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 刷新本地缓存
     * @param event
     */
    @EventListener
    @Async
    public void onEvent(SearchParamsChangeEvent event) {
        init();
    }
}

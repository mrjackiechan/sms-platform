package com.qianfeng.smsplatform.publicparam.pojo;

import com.qianfeng.smsplatform.common.check.CheckNulll;

import java.io.Serializable;
import java.util.Date;

public class TPublicParams implements Serializable,CheckNulll {
    private Integer id;

    private String paramName;

    private String paramType;

    private Short isMust;

    private Short enableState;

    private Date createDate;

    private String descripton;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName == null ? null : paramName.trim();
    }

    public String getParamType() {
        return paramType;
    }

    public void setParamType(String paramType) {
        this.paramType = paramType == null ? null : paramType.trim();
    }

    public Short getIsMust() {
        return isMust;
    }

    public void setIsMust(Short isMust) {
        this.isMust = isMust;
    }

    public Short getEnableState() {
        return enableState;
    }

    public void setEnableState(Short enableState) {
        this.enableState = enableState;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getDescripton() {
        return descripton;
    }

    public void setDescripton(String descripton) {
        this.descripton = descripton == null ? null : descripton.trim();
    }
}
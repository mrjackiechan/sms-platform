package com.qianfeng.smsplatform.publicparam.dao;

import com.qianfeng.smsplatform.publicparam.pojo.TPublicParams;
import com.qianfeng.smsplatform.publicparam.pojo.TPublicParamsExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TPublicParamsMapper {
    long countByExample(TPublicParamsExample example);

    int deleteByExample(TPublicParamsExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(TPublicParams record);

    int insertSelective(TPublicParams record);

    List<TPublicParams> selectByExample(TPublicParamsExample example);

    TPublicParams selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") TPublicParams record, @Param("example") TPublicParamsExample example);

    int updateByExample(@Param("record") TPublicParams record, @Param("example") TPublicParamsExample example);

    int updateByPrimaryKeySelective(TPublicParams record);

    int updateByPrimaryKey(TPublicParams record);
}
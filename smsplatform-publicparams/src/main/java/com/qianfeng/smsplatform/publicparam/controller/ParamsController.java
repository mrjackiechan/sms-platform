package com.qianfeng.smsplatform.publicparam.controller;

//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼                  BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  
//  


import com.github.pagehelper.PageInfo;
import com.qianfeng.smsplatform.common.dto.QueryDTO;
import com.qianfeng.smsplatform.common.dto.R;
import com.qianfeng.smsplatform.publicparam.pojo.TPublicParams;
import com.qianfeng.smsplatform.publicparam.service.ParamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Jackiechan on 2021/11/20/下午8:03
 *
 * @author Jackiechan
 * @version 1.0
 * 人学始知道，不学非自然。
 * @since 1.0
 */
@RestController
@RequestMapping("/params")
public class ParamsController {

    private ParamService paramService;


    @Autowired
    public void setParamService(ParamService paramService) {
        this.paramService = paramService;
    }

    @PostMapping("/save")
    public R addParam(@RequestBody TPublicParams publicParams) {

        paramService.addParam(publicParams);
        return R.ok();
    }

    @PostMapping("/update")
    public R updateParam(@RequestBody TPublicParams publicParams) {
        paramService.updateParam(publicParams);
        return R.ok();
    }

    @PostMapping("/del")
    public R deleteParams(@RequestBody List<Integer> ids) {
        paramService.deleteParams(ids);
        return R.ok();
    }

    @GetMapping("/all")
    public List<TPublicParams> findAllParams() {
        return paramService.findAllParams();
    }

    @GetMapping("/all/enable")
    List<TPublicParams> findAllEnableParams() {
        return paramService.findAllEnableParams();

    }

    @GetMapping("/all/must")
    List<TPublicParams> findAllMustParams() {
        return paramService.findAllMustParams();
    }
    @GetMapping("/info/{id}")
    public TPublicParams findById(@PathVariable Integer id) {
        return paramService.findById(id);
    }


    @PostMapping("/list")
    public PageInfo findByPage(@RequestBody QueryDTO queryDTO) {
        PageInfo pageInfo = paramService.findByPage(queryDTO);
        //return R.ok(pageInfo);
        return pageInfo;
    }
}

package com.qianfeng.smsplatform.publicparam.service.impl;

//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼                  BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  
//  


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qianfeng.smsplatform.common.check.CheckType;
import com.qianfeng.smsplatform.common.constants.InterfaceExceptionDict;
import com.qianfeng.smsplatform.common.dto.QueryDTO;
import com.qianfeng.smsplatform.common.exceptions.AddDataErrorException;
import com.qianfeng.smsplatform.common.exceptions.DeleteDataErrorException;
import com.qianfeng.smsplatform.common.exceptions.QueryDataErrorException;
import com.qianfeng.smsplatform.common.exceptions.UpdateDataErrorException;
import com.qianfeng.smsplatform.publicparam.dao.TPublicParamsMapper;
import com.qianfeng.smsplatform.publicparam.pojo.TPublicParams;
import com.qianfeng.smsplatform.publicparam.pojo.TPublicParamsExample;
import com.qianfeng.smsplatform.publicparam.service.ParamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * Created by Jackiechan on 2021/11/20/下午4:18
 *
 * @author Jackiechan
 * @version 1.0
 * 人学始知道，不学非自然。
 * @since 1.0
 */
@Service
@Transactional
public class ParamServiceImpl implements ParamService {

    private TPublicParamsMapper paramsMapper;

    @Autowired
    public void setParamsMapper(TPublicParamsMapper paramsMapper) {
        this.paramsMapper = paramsMapper;
    }

    @Override
    public void addParam(TPublicParams publicParams) {
        if (publicParams.isNull(CheckType.ADD)) {
            throw new AddDataErrorException("数据为空", InterfaceExceptionDict.RETURN_STATUS_ERROR);
        }
        paramsMapper.insertSelective(publicParams);
    }

    @Override
    public void updateParam(TPublicParams publicParams) {
        if (publicParams.isNull(CheckType.UPDATE)) {
            throw new UpdateDataErrorException("数据为空", InterfaceExceptionDict.RETURN_STATUS_ERROR);
        }
        paramsMapper.updateByPrimaryKey(publicParams);
    }

    @Override
    public void deleteParams(List<Integer> ids) {
        if (ObjectUtils.isEmpty(ids)) {
            throw new DeleteDataErrorException("数据为空", InterfaceExceptionDict.RETURN_STATUS_ERROR);
        }
        TPublicParamsExample example = new TPublicParamsExample();
        example.createCriteria().andIdIn(ids);
        TPublicParams publicParams = new TPublicParams();
        publicParams.setEnableState((short) 0);
        paramsMapper.updateByExampleSelective(publicParams,example);
    }

    @Override
    public TPublicParams findById(Integer id) {
        if (id == null || id <= 0) {
            throw new QueryDataErrorException("主键不合法", InterfaceExceptionDict.RETURN_STATUS_ERROR);
        }
        return paramsMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<TPublicParams> findAllParams() {
        TPublicParamsExample example = new TPublicParamsExample();
        return paramsMapper.selectByExample(example);
    }

    @Override
    public List<TPublicParams> findAllEnableParams() {
        TPublicParamsExample example = new TPublicParamsExample();
        example.createCriteria().andEnableStateEqualTo((short) 1);
        return paramsMapper.selectByExample(example);
    }

    @Override
    public List<TPublicParams> findAllMustParams() {
        TPublicParamsExample example = new TPublicParamsExample();
        example.createCriteria().andEnableStateEqualTo((short) 1).andIsMustEqualTo((short) 1);
        return paramsMapper.selectByExample(example);
    }

    @Override
    public PageInfo findByPage(QueryDTO queryDTO) {
        PageHelper.offsetPage(queryDTO.getOffset(), queryDTO.getLimit());
        TPublicParamsExample example = new TPublicParamsExample();
        String sort = queryDTO.getSort();
        if (!StringUtils.isEmpty(sort)) {
            example.setOrderByClause("id");
        }
        List<TPublicParams> tPublicParams = paramsMapper.selectByExample(example);
        return new PageInfo(tPublicParams);
    }

}

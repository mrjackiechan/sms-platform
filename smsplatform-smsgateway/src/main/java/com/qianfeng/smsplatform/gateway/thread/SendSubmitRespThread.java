package com.qianfeng.smsplatform.gateway.thread;

import com.qianfeng.smsplatform.common.model.Standard_Submit;
import com.qianfeng.smsplatform.gateway.mq.MTQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;

public class SendSubmitRespThread implements Runnable{
    private final static Logger log = LoggerFactory.getLogger(SendSubmitRespThread.class);
    @Autowired
    private AmqpTemplate rabbitTemplate;

    private String topic;

    private MTQueue mtqueue = MTQueue.getInstance();

    public SendSubmitRespThread(String topic,AmqpTemplate rabbitTemplate){
        this.topic = topic;
        this.rabbitTemplate = rabbitTemplate;
    }

    @Override
    public void run() {
        while (true) {
            try {
                if (mtqueue.size() > 0) {
                    Standard_Submit resp = (Standard_Submit) mtqueue.remove();
                  //  resp.setErrorCode(InterfaceExceptionDict.RETURN_STATUS_SUCCESS);//设置成功
                   // resp.setUpdateTime(new Date());//设置更新日志为当前时间
                    resp.setReportState(1);//设置状态为1 也就是发送中
                    rabbitTemplate.convertAndSend(topic, resp);//将消息发送到es中作为日志保存
                } else {
                    sleep(2000);
                }
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
                sleep(2000);
            }
        }
    }

    private void sleep(long time){
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }
    }
}

package com.qianfeng.smsplatform.smstemp.mapper;

import com.qianfeng.smsplatform.smstemp.pojo.TSmsTemplate;
import com.qianfeng.smsplatform.smstemp.pojo.TSmsTemplateExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TSmsTemplateMapper {
    long countByExample(TSmsTemplateExample example);

    int deleteByExample(TSmsTemplateExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(TSmsTemplate record);

    int insertSelective(TSmsTemplate record);

    List<TSmsTemplate> selectByExampleWithBLOBs(TSmsTemplateExample example);

    List<TSmsTemplate> selectByExample(TSmsTemplateExample example);

    TSmsTemplate selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") TSmsTemplate record, @Param("example") TSmsTemplateExample example);

    int updateByExampleWithBLOBs(@Param("record") TSmsTemplate record, @Param("example") TSmsTemplateExample example);

    int updateByExample(@Param("record") TSmsTemplate record, @Param("example") TSmsTemplateExample example);

    int updateByPrimaryKeySelective(TSmsTemplate record);

    int updateByPrimaryKeyWithBLOBs(TSmsTemplate record);

    int updateByPrimaryKey(TSmsTemplate record);
}
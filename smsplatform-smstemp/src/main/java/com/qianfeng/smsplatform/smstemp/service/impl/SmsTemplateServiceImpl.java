package com.qianfeng.smsplatform.smstemp.service.impl;

//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼                  BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  
//  


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qianfeng.smsplatform.common.check.CheckType;
import com.qianfeng.smsplatform.common.dto.QueryDTO;
import com.qianfeng.smsplatform.common.exceptions.AddDataErrorException;
import com.qianfeng.smsplatform.common.exceptions.DeleteDataErrorException;
import com.qianfeng.smsplatform.common.exceptions.UpdateDataErrorException;
import com.qianfeng.smsplatform.smstemp.mapper.TSmsTemplateMapper;
import com.qianfeng.smsplatform.smstemp.pojo.TSmsTemplate;
import com.qianfeng.smsplatform.smstemp.pojo.TSmsTemplateExample;
import com.qianfeng.smsplatform.smstemp.service.SmsTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by jackiechan on 2021/10/24/下午7:23
 *
 * @Author jackiechan
 */
@Service
@Transactional
public class SmsTemplateServiceImpl implements SmsTemplateService {

    private TSmsTemplateMapper smsTemplateMapper;

    @Autowired
    public void setSmsTemplateMapper(TSmsTemplateMapper smsTemplateMapper) {
        this.smsTemplateMapper = smsTemplateMapper;
    }


    @Override
    public int addSmsTemplate(TSmsTemplate smsTemplate) {
        if (smsTemplate.isNull(CheckType.ADD)) {
            throw new AddDataErrorException("必须传递的数据为空", -1);
        }
        return smsTemplateMapper.insert(smsTemplate);
    }

    @Override
    public int delSmsTemplate(List<Integer> id) {
        if (id==null||id.size()==0) {
            throw new DeleteDataErrorException("必须传递ID", -1);
        }
        TSmsTemplate smsTemplate = new TSmsTemplate();
        smsTemplate.setStatus((short) 0);
        TSmsTemplateExample example = new TSmsTemplateExample();
        TSmsTemplateExample.Criteria criteria = example.createCriteria().andIdIn(id);
        return smsTemplateMapper.updateByExampleSelective(smsTemplate,example);
    }

    @Override
    public int updateSmsTemplate(TSmsTemplate smsTemplate) {
        if (smsTemplate.isNull(CheckType.UPDATE)) {
            throw new UpdateDataErrorException("更新数据需要填写内容", -1);

        }
        return smsTemplateMapper.updateByPrimaryKeyWithBLOBs(smsTemplate);
    }

    @Override
    public TSmsTemplate findById(Integer id) {
        return smsTemplateMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<TSmsTemplate> findAll() {
        return smsTemplateMapper.selectByExampleWithBLOBs(new TSmsTemplateExample());
    }

    @Override
    public PageInfo findByPage(QueryDTO queryDTO) {
        PageHelper.offsetPage(queryDTO.getOffset(), queryDTO.getLimit());
        List<TSmsTemplate> tSmsTemplates = smsTemplateMapper.selectByExampleWithBLOBs(new TSmsTemplateExample());
        PageInfo<TSmsTemplate> info = new PageInfo<>(tSmsTemplates);
        return info;

    }
}

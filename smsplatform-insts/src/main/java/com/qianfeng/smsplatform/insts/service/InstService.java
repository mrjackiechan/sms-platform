package com.qianfeng.smsplatform.insts.service;


import com.qianfeng.smsplatform.insts.bean.TInst;

import java.util.List;

public interface InstService {
    //查询所有的省
    public List<TInst> findProvs();
    //查询省对应的市
    public List<TInst> findCitys(Long provId);


    TInst findById(Long provId);

    List<TInst> findByParentIdAndCityId(Long provId, Long cityId);

}

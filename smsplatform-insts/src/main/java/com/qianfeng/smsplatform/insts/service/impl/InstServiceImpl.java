package com.qianfeng.smsplatform.insts.service.impl;

import com.qianfeng.smsplatform.insts.bean.TInst;
import com.qianfeng.smsplatform.insts.bean.TInstExample;
import com.qianfeng.smsplatform.insts.dao.TInstMapper;
import com.qianfeng.smsplatform.insts.service.InstService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional
public class InstServiceImpl implements InstService {

    @Autowired
    private TInstMapper tInstMapper;

    @Override
    @Transactional(readOnly = true,propagation = Propagation.SUPPORTS)
    public List<TInst> findProvs() {
        TInstExample tInstExample = new TInstExample();
        TInstExample.Criteria criteria = tInstExample.createCriteria();
        criteria.andParentidEqualTo(1L);
        List<TInst> tInsts = tInstMapper.selectByExample(tInstExample);
        return tInsts;
    }

    @Override
    @Transactional(readOnly = true,propagation = Propagation.SUPPORTS)
    public List<TInst> findCitys(Long provId) {
        TInstExample tInstExample = new TInstExample();
        TInstExample.Criteria criteria = tInstExample.createCriteria();
        criteria.andParentidEqualTo(provId);
        List<TInst> tInsts = tInstMapper.selectByExample(tInstExample);
        return tInsts;
    }

    @Override
    public TInst findById(Long provId) {
        return  tInstMapper.selectByPrimaryKey(provId);
    }

    @Override
    public List<TInst> findByParentIdAndCityId(Long provId, Long cityId) {
        TInstExample tInstExample = new TInstExample();
        TInstExample.Criteria criteria = tInstExample.createCriteria();
        criteria.andIdEqualTo(cityId);
        criteria.andParentidEqualTo(provId);
        List<TInst> tInsts = tInstMapper.selectByExample(tInstExample);
        return tInsts;
    }
}

package com.qianfeng.smsplatform.insts.controller;

import com.qianfeng.smsplatform.insts.bean.TInst;
import com.qianfeng.smsplatform.insts.service.InstService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@ResponseBody
@RequestMapping("/insts")
public class InstController {

    private InstService instService;

    @Autowired
    public void setInstService(InstService instService) {
        this.instService = instService;
    }

    @RequestMapping("/all")
    public List<TInst> findProvs() {
        List<TInst> provs = instService.findProvs();
        return provs;
    }


    @RequestMapping("/all/{provId}")
    public List<TInst>findCitys(@PathVariable("provId") Long provId) {
        List<TInst> provs = instService.findCitys(provId);
        return provs;
    }


    @GetMapping("/info/{provId}")
    TInst findById(@PathVariable Long provId){
        return instService.findById(provId);
    }

    @GetMapping("/all/{provId}/{cityId}")
    List<TInst> findByParentIdAndCityId(@PathVariable Long provId, @PathVariable Long cityId){
        return instService.findByParentIdAndCityId(provId, cityId);
    }
}

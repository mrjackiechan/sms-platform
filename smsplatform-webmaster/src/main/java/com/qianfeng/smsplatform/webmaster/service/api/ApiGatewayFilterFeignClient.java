package com.qianfeng.smsplatform.webmaster.service.api;
//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼                  BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  
//  


import com.github.pagehelper.PageInfo;
import com.qianfeng.smsplatform.common.dto.QueryDTO;
import com.qianfeng.smsplatform.common.dto.R;
import com.qianfeng.smsplatform.webmaster.pojo.TApiGateWayFilter;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Jackiechan on 2021/11/14/下午9:18
 *
 * @author Jackiechan
 * @version 1.0
 * @since 1.0
 */
@RequestMapping("/apigatewayfilters")
@FeignClient("smsplatform-apigatewayfilter")
public interface ApiGatewayFilterFeignClient {


    /**
     * 修改某个分组的过滤器的顺序
     *
     * @return
     */
    @PostMapping("/update")
    R updateFilters(@RequestBody TApiGateWayFilter filter);

    /**
     * 添加一个过滤器的分组
     *
     * @param filter 过滤器
     * @return
     */
    @PostMapping("/save")
    R addFilters(@RequestBody TApiGateWayFilter filter);

    @PostMapping("/del")
    R deleteFilters(@RequestBody List<Integer> ids);

    @GetMapping("/info/{id}")
    TApiGateWayFilter findById(@PathVariable Integer id);

    @GetMapping("/all")
    List<TApiGateWayFilter> findAll();

    @PostMapping("/list")
    PageInfo findByPage(@RequestBody QueryDTO queryDTO);

    @GetMapping("/state")
    List<TApiGateWayFilter> findByState(@RequestParam(defaultValue = "1") short state);
}

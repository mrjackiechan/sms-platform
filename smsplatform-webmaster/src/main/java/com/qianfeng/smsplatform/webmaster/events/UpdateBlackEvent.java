package com.qianfeng.smsplatform.webmaster.events;


//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼            BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  


import com.qianfeng.smsplatform.webmaster.pojo.TBlackList;

import java.util.List;

/**
 * Created by jackiechan on 2021/8/1 11:35
 *
 * @author jackiechan
 * 人学始知道，不学非自然。
 */
public class UpdateBlackEvent {

    private EventType eventType;//事件类型
    private List<TBlackList> tBlackList;//操作的黑名单

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public List<TBlackList> gettBlackList() {
        return tBlackList;
    }

    public void settBlackList(List<TBlackList> tBlackList) {
        this.tBlackList = tBlackList;
    }

    public UpdateBlackEvent() {
    }

    public UpdateBlackEvent(EventType eventType, List<TBlackList> tBlackList ) {
        this.eventType = eventType;
        this.tBlackList = tBlackList;
    }

    public static UpdateBlackEvent createEvent(EventType eventType, List<TBlackList> tBlackList) {
        UpdateBlackEvent updateBlackEvent = new UpdateBlackEvent(eventType, tBlackList);
        return updateBlackEvent;
    }
}

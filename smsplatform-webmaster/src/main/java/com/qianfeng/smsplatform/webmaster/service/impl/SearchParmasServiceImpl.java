package com.qianfeng.smsplatform.webmaster.service.impl;

//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼                  BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  
//  


import com.github.pagehelper.PageInfo;
import com.qianfeng.smsplatform.common.check.CheckType;
import com.qianfeng.smsplatform.common.constants.InterfaceExceptionDict;
import com.qianfeng.smsplatform.common.dto.R;
import com.qianfeng.smsplatform.common.dto.QueryDTO;
import com.qianfeng.smsplatform.webmaster.events.SearchParmasEvent;
import com.qianfeng.smsplatform.common.exceptions.DeleteDataErrorException;
import com.qianfeng.smsplatform.common.exceptions.QueryDataErrorException;
import com.qianfeng.smsplatform.common.exceptions.UpdateDataErrorException;
import com.qianfeng.smsplatform.webmaster.pojo.TSearchParmas;
import com.qianfeng.smsplatform.webmaster.service.SearchParmasService;
import com.qianfeng.smsplatform.webmaster.service.api.SearchParamsFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Administrator on 2021/8/9/18:51
 *
 * @author Administrator
 * @version 1.0
 * @since 1.0
 */
@Service
@Transactional
public class SearchParmasServiceImpl implements SearchParmasService {


    private ApplicationContext context;

    @Autowired
    public void setContext(ApplicationContext context) {
        this.context = context;
    }

    private SearchParamsFeign searchParamsFeign;

    @Autowired
    public void setSearchParamsFeign(SearchParamsFeign searchParamsFeign) {
        this.searchParamsFeign = searchParamsFeign;
    }

    @Override
    public R updateSearchParmas(TSearchParmas searchParmas) {
        if (searchParmas.isNull(CheckType.UPDATE)) {
            throw new UpdateDataErrorException("没有传递主键", 500);
        }
       // int update = searchParmasMapper.updateByPrimaryKeySelective(searchParmas);//更新数据
        R result = searchParamsFeign.updatesearchparams(searchParmas);
        //发送事件更新搜索参数缓存
        syncSearchParmas();
        return result;
    }

    @Override
    public R addSearchParmas(TSearchParmas searchParmas) {
        if (searchParmas.isNull(CheckType.ADD)) {
            throw new UpdateDataErrorException("数据不完整", 500);
        }
        R result = searchParamsFeign.addsearchparams(searchParmas);
        //发送事件更新缓存
        if (1 == searchParmas.getState()&& result.getCode()== InterfaceExceptionDict.RETURN_STATUS_SUCCESS) {
            syncSearchParmas();
        }
        return result;
    }

    @Override
    public R deleteSearchParmas(List<Integer> ids) {
        if (ids == null || ids.size() == 0) {
            throw new DeleteDataErrorException("没有传递id", 500);
        }
        R result = searchParamsFeign.delsearchparamss(ids);
        //发送事件更新数据
        if (result.getCode() == InterfaceExceptionDict.RETURN_STATUS_SUCCESS) {
            syncSearchParmas();
        }
        return result;
    }

    @Override
    public TSearchParmas findById(Integer id) {
        if (id == null || id <= 0) {
            throw new QueryDataErrorException("没有传递id", 500);
        }
        TSearchParmas searchParmas = searchParamsFeign.findById(id);
        return searchParmas;
    }

    @Override
    public List<TSearchParmas> findAll() {
        List<TSearchParmas> parmasList = searchParamsFeign.findAll();
        return parmasList;
    }

    @Override
    public PageInfo findByPage(QueryDTO queryDTO) {
        PageInfo<TSearchParmas> info = searchParamsFeign.findByPage(queryDTO);
        return info;
    }

    @Override
    public List<TSearchParmas> findByState(Short state) {
        if (state == null) {
            return null;
        }
        List<TSearchParmas> tSearchParmasList = searchParamsFeign.findByState(state);
        return tSearchParmasList;
    }

    @Override
    public void syncSearchParmas() {
        List<TSearchParmas> parmasList = findByState((short) 1);//查询启用的参数
        context.publishEvent(SearchParmasEvent.createEvent(parmasList));
    }
}

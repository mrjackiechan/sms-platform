package com.qianfeng.smsplatform.webmaster.service;

import com.github.pagehelper.PageInfo;
import com.qianfeng.smsplatform.webmaster.dto.DataGridResult;
import com.qianfeng.smsplatform.common.dto.QueryDTO;
import com.qianfeng.smsplatform.webmaster.pojo.TPhase;

import java.util.List;

public interface PhaseService {
    public int addPhase(TPhase tPhase);
    public int delPhase(Long id);
    public int updatePhase(TPhase tPhase);
    public TPhase findById(Long id);
    public List<TPhase> findALL();

    public PageInfo findByPage(QueryDTO queryDTO);


    void syncPhase();
}

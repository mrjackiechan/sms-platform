package com.qianfeng.smsplatform.webmaster.service;

import com.github.pagehelper.PageInfo;
import com.qianfeng.smsplatform.webmaster.pojo.TApiMapping;

import java.util.Map;

public interface ApiMappingService {
    void addApiMapping(TApiMapping mapping);

    void updateApiMapping(TApiMapping mapping);

    PageInfo getMappingList(Map criteria, int offset, int limit);

    TApiMapping getMappingById(Integer id);

    void deleteMapping(Integer[] ids);
    void syncCache();
}

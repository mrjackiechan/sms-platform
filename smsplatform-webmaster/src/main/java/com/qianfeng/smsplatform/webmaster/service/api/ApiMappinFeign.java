package com.qianfeng.smsplatform.webmaster.service.api;
//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼                  BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  
//  


import com.github.pagehelper.PageInfo;
import com.qianfeng.smsplatform.common.dto.R;
import com.qianfeng.smsplatform.webmaster.pojo.TApiMapping;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * Created by Jackiechan on 2021/11/13/下午4:21
 *
 * @author Jackiechan
 * @version 1.0
 * @since 1.0
 */
@FeignClient("smsplatform-apimapping")
@RequestMapping("/apimapping")
public interface ApiMappinFeign {
    @RequestMapping("/list")
     PageInfo table(@RequestBody Map params, @RequestParam(defaultValue = "1") Integer offset, @RequestParam(defaultValue = "10") Integer limit);

    @RequestMapping("/save")
     R add(@RequestBody TApiMapping tApiMapping);

    @RequestMapping("/update")
     R update(@RequestBody TApiMapping tApiMapping) ;

    @RequestMapping("/info/{id}")
    TApiMapping info(@PathVariable Integer id);

    @RequestMapping("/del")
     R delete(@RequestBody Integer[] ids) ;

    @RequestMapping("/allbystate")
    List<TApiMapping> findAll(Byte state);
}

package com.qianfeng.smsplatform.webmaster.mq;


//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼            BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qianfeng.smsplatform.webmaster.pojo.TAcountRecord;
import com.qianfeng.smsplatform.webmaster.service.AcountRecordService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Map;

/**
 * Created by jackiechan on 2021/7/26 10:55
 *
 * @author jackiechan
 * 人学始知道，不学非自然。
 */

@Component
public class PaymentResultListener {
    private ObjectMapper objectMapper;
    @Autowired
    public void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    private AcountRecordService acountRecordService;

    @Autowired
    public void setAcountRecordService(AcountRecordService acountRecordService) {
        this.acountRecordService = acountRecordService;
    }

    @RabbitListener(queues = "PAYRESULTQUEUE_WEBMASTER")
    public void onMessage(String json) {
        System.err.println("webmaster收到的数据是:" + json);
        Map map = null;
        try {
            map = objectMapper.readValue(json, Map.class);
            TAcountRecord tAcountRecord = new TAcountRecord();
            tAcountRecord.setPaytime(new Date());
            tAcountRecord.setPaymentid((Integer) map.get("paymentid"));
            tAcountRecord.setPaymentorder((String) map.get("transaction_id"));//设置微信的订单号
            tAcountRecord.setPaymentinfo("success");
            tAcountRecord.setOrderid((String) map.get("out_trade_no"));//设置我们的订单号,我们需要根据这个订单号更新数据
            acountRecordService.updateByorderid(tAcountRecord);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }




    }
}

package com.qianfeng.smsplatform.webmaster.service.impl;

//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼                  BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  
//  


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qianfeng.smsplatform.common.check.CheckType;
import com.qianfeng.smsplatform.common.dto.R;
import com.qianfeng.smsplatform.common.dto.QueryDTO;
import com.qianfeng.smsplatform.common.exceptions.AddDataErrorException;
import com.qianfeng.smsplatform.common.exceptions.DeleteDataErrorException;
import com.qianfeng.smsplatform.common.exceptions.UpdateDataErrorException;
import com.qianfeng.smsplatform.webmaster.pojo.*;
import com.qianfeng.smsplatform.webmaster.service.SmsTemplateService;
import com.qianfeng.smsplatform.webmaster.service.api.SmsTempFeignClient;
import com.qianfeng.smsplatform.webmaster.util.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by jackiechan on 2021/10/24/下午7:23
 *
 * @Author jackiechan
 */
@Service
@Transactional
public class SmsTemplateServiceImpl implements SmsTemplateService {

    private SmsTempFeignClient smsTempFeignClient;

    @Autowired
    public void setSmsTempFeignClient(SmsTempFeignClient smsTempFeignClient) {
        this.smsTempFeignClient = smsTempFeignClient;
    }

    @Override
    public R addSmsTemplate(TSmsTemplate smsTemplate) {
        if (smsTemplate.isNull(CheckType.ADD)) {
            throw new AddDataErrorException("必须传递的数据为空", -1);
        }
        TAdminUser userEntity = ShiroUtils.getUserEntity();
        Integer userid = userEntity.getId();//用户id
        Short userType = userEntity.getType();//用户类型
        smsTemplate.setOwntype(userType);
        smsTemplate.setCreater(userid);
        return smsTempFeignClient.addsmstemp(smsTemplate);
    }

    @Override
    public R delSmsTemplate(List<Integer> id) {
        if (id==null||id.size()==0) {
            throw new DeleteDataErrorException("必须传递ID", -1);
        }
        return smsTempFeignClient.delsmstemp(id);
    }

    @Override
    public R updateSmsTemplate(TSmsTemplate smsTemplate) {
        if (smsTemplate.isNull(CheckType.UPDATE)) {
            throw new UpdateDataErrorException("更新数据需要填写内容", -1);

        }
        return smsTempFeignClient.updatesmstemp(smsTemplate);
    }

    @Override
    public TSmsTemplate findById(Integer id) {
        return smsTempFeignClient.findById(id);
    }

    @Override
    public List<TSmsTemplate> findAll() {
        return smsTempFeignClient.findAll();
    }

    @Override
    public PageInfo findByPage(QueryDTO queryDTO) {
        PageInfo pageInfo = smsTempFeignClient.findByPage(queryDTO);
        return pageInfo;

    }
}

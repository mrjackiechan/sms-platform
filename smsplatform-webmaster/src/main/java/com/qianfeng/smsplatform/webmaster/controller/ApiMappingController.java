package com.qianfeng.smsplatform.webmaster.controller;

import com.github.pagehelper.PageInfo;
import com.qianfeng.smsplatform.webmaster.dto.AjaxMessage;
import com.qianfeng.smsplatform.webmaster.dto.DataGridResult;
import com.qianfeng.smsplatform.common.dto.R;
import com.qianfeng.smsplatform.webmaster.pojo.TApiMapping;
import com.qianfeng.smsplatform.webmaster.service.ApiMappingService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * 路由管理
 */
@RestController

public class ApiMappingController {

    @Autowired
    private ApiMappingService apiMappingService;

    @RequestMapping("/sys/apimapping/list")
    public R table(Map params, Integer offset, Integer limit) {
        PageInfo pageInfo = apiMappingService.getMappingList(params, offset, limit);
        R ok = R.ok();
        ok.put("total", pageInfo.getTotal());
        ok.put("rows", pageInfo.getList());
        return ok;
    }

    @RequestMapping("/sys/apimapping/save")
    public R add(@RequestBody TApiMapping tApiMapping) {
        apiMappingService.addApiMapping(tApiMapping);
        return R.ok();
    }

    @RequestMapping("/sys/apimapping/update")
    public R update(@RequestBody TApiMapping tApiMapping) {
        apiMappingService.updateApiMapping(tApiMapping);
        return R.ok();
    }

    @RequestMapping("/sys/apimapping/info/{id}")
    public R info(@PathVariable Integer id) {
        TApiMapping mapping = apiMappingService.getMappingById(id);
        return R.ok().put("apimapping", mapping);
    }

    @RequestMapping("/sys/apimapping/del")
    public R delete(@RequestBody Integer[] ids) {
        apiMappingService.deleteMapping(ids);
        return R.ok();
    }

}

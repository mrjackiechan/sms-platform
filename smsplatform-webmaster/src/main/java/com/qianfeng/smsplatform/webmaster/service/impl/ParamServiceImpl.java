package com.qianfeng.smsplatform.webmaster.service.impl;

//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼                  BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  
//  


import com.github.pagehelper.PageInfo;
import com.qianfeng.smsplatform.common.check.CheckType;
import com.qianfeng.smsplatform.common.constants.InterfaceExceptionDict;
import com.qianfeng.smsplatform.common.dto.QueryDTO;
import com.qianfeng.smsplatform.common.dto.R;
import com.qianfeng.smsplatform.common.exceptions.AddDataErrorException;
import com.qianfeng.smsplatform.common.exceptions.DeleteDataErrorException;
import com.qianfeng.smsplatform.common.exceptions.QueryDataErrorException;
import com.qianfeng.smsplatform.common.exceptions.UpdateDataErrorException;
import com.qianfeng.smsplatform.webmaster.events.UpdatePublicParamsEvent;
import com.qianfeng.smsplatform.webmaster.pojo.TPublicParams;
import com.qianfeng.smsplatform.webmaster.service.ParamService;
import com.qianfeng.smsplatform.webmaster.service.api.PublicParamsFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.List;

/**
 * Created by Jackiechan on 2021/11/20/下午4:18
 *
 * @author Jackiechan
 * @version 1.0
 * 人学始知道，不学非自然。
 * @since 1.0
 */
@Service
@Transactional
public class ParamServiceImpl implements ParamService {


    private ApplicationContext context;


    @Autowired
    public void setContext(ApplicationContext context) {
        this.context = context;
    }

    private PublicParamsFeign publicParamsFeign;

    @Autowired
    public void setPublicParamsFeign(PublicParamsFeign publicParamsFeign) {
        this.publicParamsFeign = publicParamsFeign;
    }

    @Override
    public R addParam(TPublicParams publicParams) {
        if (publicParams.isNull(CheckType.ADD)) {
            throw new AddDataErrorException("数据为空", InterfaceExceptionDict.RETURN_STATUS_ERROR);
        }
        R result = publicParamsFeign.addParam(publicParams);
        if (publicParams.getIsMust() == 1 && result.getCode() == InterfaceExceptionDict.RETURN_STATUS_SUCCESS) {
          //  context.publishEvent(new UpdatePublicParamsEvent());//发布更新参数的事件
            syncParamsCache();
        }
        return result;
    }

    @Override
    public R updateParam(TPublicParams publicParams) {
        if (publicParams.isNull(CheckType.UPDATE)) {
            throw new UpdateDataErrorException("数据为空", InterfaceExceptionDict.RETURN_STATUS_ERROR);
        }

        R result =  publicParamsFeign.updateParam(publicParams);
        if (publicParams.getIsMust() == 1 && result.getCode() == InterfaceExceptionDict.RETURN_STATUS_SUCCESS) {
            //  context.publishEvent(new UpdatePublicParamsEvent());//发布更新参数的事件
            syncParamsCache();
        }
        return result;
    }

    @Override
    public R deleteParams(List<Integer> ids) {
        if (ObjectUtils.isEmpty(ids)) {
            throw new DeleteDataErrorException("数据为空", InterfaceExceptionDict.RETURN_STATUS_ERROR);
        }
        R result= publicParamsFeign.deleteParams(ids);
        if (result.getCode() == InterfaceExceptionDict.RETURN_STATUS_SUCCESS) {
            //  context.publishEvent(new UpdatePublicParamsEvent());//发布更新参数的事件
            syncParamsCache();
        }
        return result;
    }

    @Override
    public TPublicParams findById(Integer id) {
        if (id == null || id <= 0) {
            throw new QueryDataErrorException("主键不合法", InterfaceExceptionDict.RETURN_STATUS_ERROR);
        }
        return publicParamsFeign.findById(id);
    }

    @Override
    public List<TPublicParams> findAllParams() {
        return publicParamsFeign.findAllParams();
    }

    @Override
    public List<TPublicParams> findAllEnableParams() {
        return publicParamsFeign.findAllEnableParams();
    }

    @Override
    public List<TPublicParams> findAllMustParams() {
        return publicParamsFeign.findAllMustParams();
    }

    @Override
    public PageInfo findByPage(QueryDTO queryDTO) {
        return publicParamsFeign.findByPage(queryDTO);
    }


    @Override
    public void syncParamsCache() {
        List<TPublicParams> allMustParams = findAllMustParams();
        context.publishEvent(new UpdatePublicParamsEvent(allMustParams));//发布更新参数的事件
    }

}

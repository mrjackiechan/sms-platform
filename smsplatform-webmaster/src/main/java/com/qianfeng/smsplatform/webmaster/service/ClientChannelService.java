package com.qianfeng.smsplatform.webmaster.service;

import com.github.pagehelper.PageInfo;
import com.qianfeng.smsplatform.webmaster.dto.DataGridResult;
import com.qianfeng.smsplatform.common.dto.QueryDTO;
import com.qianfeng.smsplatform.webmaster.pojo.TClientChannel;

import java.util.List;

public interface ClientChannelService {
    public int addClientChannel(TClientChannel tClientChannel);

    public int delClientChannel(Long id);

    public int updateClientChannel(TClientChannel tClientChannel);

    public TClientChannel findById(Long id);

    public List<TClientChannel> findAll();

    public PageInfo findByPage(QueryDTO queryDTO);

    /**
     * 同步所有数据
     * @return
     */
    boolean syncAllDataToCache();
}

package com.qianfeng.smsplatform.webmaster.service.impl;

//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼                  BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  
//  


import com.github.pagehelper.PageInfo;
import com.qianfeng.smsplatform.common.check.CheckType;
import com.qianfeng.smsplatform.common.constants.InterfaceExceptionDict;
import com.qianfeng.smsplatform.common.dto.QueryDTO;
import com.qianfeng.smsplatform.common.dto.R;
import com.qianfeng.smsplatform.common.exceptions.AddDataErrorException;
import com.qianfeng.smsplatform.common.exceptions.DeleteDataErrorException;
import com.qianfeng.smsplatform.common.exceptions.QueryDataErrorException;
import com.qianfeng.smsplatform.webmaster.events.UpdateGrayReleaseEvent;
import com.qianfeng.smsplatform.webmaster.pojo.TGrayReleaseConfig;
import com.qianfeng.smsplatform.webmaster.service.GrayService;
import com.qianfeng.smsplatform.webmaster.service.api.GrayReleaseConfigFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Jackiechan on 2021/11/21/下午11:05
 *
 * @author Jackiechan
 * @version 1.0
 * 人学始知道，不学非自然。
 * @since 1.0
 */
@Service
@Transactional
public class GrayServiceImpl implements GrayService {

    private ApplicationContext context;
    @Autowired
    public void setContext(ApplicationContext context) {
        this.context = context;
    }

    private GrayReleaseConfigFeign grayReleaseConfigFeign;

    @Autowired
    public void setGrayReleaseConfigFeign(GrayReleaseConfigFeign grayReleaseConfigFeign) {
        this.grayReleaseConfigFeign = grayReleaseConfigFeign;
    }


    @Override
    public R addGrayReleaseConfig(TGrayReleaseConfig TGrayReleaseConfig) {
        if (TGrayReleaseConfig.isNull(CheckType.ADD)) {
            throw new AddDataErrorException("数据不完整", 500);
        }
        R result = grayReleaseConfigFeign.addGrayReleaseConfig(TGrayReleaseConfig);
        if (TGrayReleaseConfig.getState() == 1 && result.getCode() == InterfaceExceptionDict.RETURN_STATUS_SUCCESS) {
            syncConfig();
        }
        return result;
    }

    @Override
    public R delGrayReleaseConfigs(List<Integer> ids) {
        if (ids == null || ids.size() == 0) {
            throw new DeleteDataErrorException("没有传递id", 500);
        }
        R result = grayReleaseConfigFeign.delGrayReleaseConfigs(ids);
        if (result.getCode() == InterfaceExceptionDict.RETURN_STATUS_SUCCESS) {
            syncConfig();
        }
        return result;
    }

    @Override
    public R updateGrayReleaseConfig(TGrayReleaseConfig tGrayReleaseConfig) {
        if (tGrayReleaseConfig.isNull(CheckType.UPDATE)) {
            throw new AddDataErrorException("数据不完整", 500);
        }
        R result = grayReleaseConfigFeign.updateGrayReleaseConfig(tGrayReleaseConfig);
        if (result.getCode() == InterfaceExceptionDict.RETURN_STATUS_SUCCESS) {
            syncConfig();
        }
        return result;
    }

    @Override
    public TGrayReleaseConfig findById(Integer id) {
        if (id == null || id <= 0) {
            throw new QueryDataErrorException("没有传递主键", 500);
        }
        return grayReleaseConfigFeign.findById(id);
    }

    @Override
    public List<TGrayReleaseConfig> findAll() {
        return grayReleaseConfigFeign.findAll();
    }

    @Override
    public PageInfo findByPage(QueryDTO queryDTO) {
        return grayReleaseConfigFeign.findByPage(queryDTO);
    }

    @Override
    public List<TGrayReleaseConfig> findByState(Short state) {
        return grayReleaseConfigFeign.findByState(state);
    }

    @Override
    public void syncConfig() {
        List<TGrayReleaseConfig> configList = findByState((short) 1);
        context.publishEvent(new UpdateGrayReleaseEvent(configList));
    }
}

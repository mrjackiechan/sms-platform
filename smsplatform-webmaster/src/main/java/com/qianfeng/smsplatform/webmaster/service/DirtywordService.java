package com.qianfeng.smsplatform.webmaster.service;

import com.github.pagehelper.PageInfo;
import com.qianfeng.smsplatform.webmaster.dto.DataGridResult;
import com.qianfeng.smsplatform.common.dto.QueryDTO;
import com.qianfeng.smsplatform.webmaster.pojo.TDirtyword;

import java.util.List;

public interface DirtywordService {

    public int addDirtyword(TDirtyword tDirtyword);

    public int delDirtyword(Long id);

    public int updateDirtyword(TDirtyword tDirtyword);

    public TDirtyword findById(Long id);

    public List<TDirtyword> findAll();

    public PageInfo findByPage(QueryDTO queryDTO);

    void syncDirtyword2Redis();
}

package com.qianfeng.smsplatform.webmaster.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qianfeng.smsplatform.webmaster.dao.TBlackListMapper;
import com.qianfeng.smsplatform.common.dto.QueryDTO;
import com.qianfeng.smsplatform.webmaster.events.EventType;
import com.qianfeng.smsplatform.webmaster.events.UpdateBlackEvent;
import com.qianfeng.smsplatform.webmaster.pojo.TAdminUser;
import com.qianfeng.smsplatform.webmaster.pojo.TBlackList;
import com.qianfeng.smsplatform.webmaster.pojo.TBlackListExample;
import com.qianfeng.smsplatform.webmaster.service.BlackService;
import com.qianfeng.smsplatform.webmaster.util.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.List;

@Service
@Transactional()
public class BlackServiceImpl implements BlackService {

    private TBlackListMapper tBlackListMapper;

    private ApplicationContext context;

    @Autowired
    public void setContext(ApplicationContext context) {
        this.context = context;
    }

    @Autowired
    public void settBlackListMapper(TBlackListMapper tBlackListMapper) {
        this.tBlackListMapper = tBlackListMapper;
    }


    @Override
    public int addBlack(TBlackList tBlackList) {
        TAdminUser userEntity = ShiroUtils.getUserEntity();
        Integer userid = userEntity.getId();//用户id
        Short userType = userEntity.getType();//用户类型
        tBlackList.setOwntype(userType);
        tBlackList.setCreater(userid);
        int i = tBlackListMapper.insertSelective(tBlackList);//添加到数据库
        //操作缓存不应当影响执行结果,所以通过异步执行
        // 通过事件当时发送更新缓存的消息
        context.publishEvent(UpdateBlackEvent.createEvent(EventType.ADD, Arrays.asList(tBlackList)));
        return i;
    }

    @Override
    public int delBlack(Long id) {
        TBlackList tBlackList = findById(id);
        int i = tBlackListMapper.deleteByPrimaryKey(id);//从数据库中删除数据
        //操作缓存不应当影响执行结果,所以通过异步执行
        // 通过事件当时发送更新缓存的消息
        context.publishEvent(UpdateBlackEvent.createEvent(EventType.DELETE, Arrays.asList(tBlackList)));
        return i;

    }

    @Override
    public int updateBlack(TBlackList tBlackList) {
        //TODO 更新应该先从缓存中删除原始数据,再添加更新后的数据,然后发送消息
        TBlackList source = tBlackListMapper.selectByPrimaryKey(tBlackList.getId());//先获取原始的数据,方便后面从 redis 中删除
        int i = tBlackListMapper.updateByPrimaryKey(tBlackList);
        if (i > 0) {
            //操作缓存不应当影响执行结果,所以通过异步执行
            // 通过事件当时发送更新缓存的消息
            context.publishEvent(UpdateBlackEvent.createEvent(EventType.DELETE, Arrays.asList(source)));
            context.publishEvent(UpdateBlackEvent.createEvent(EventType.UPDATE, Arrays.asList(tBlackList)));

        }
        return i;
    }

    @Override
    @Transactional(readOnly = true,propagation = Propagation.SUPPORTS)
    public TBlackList findById(Long id) {
        return tBlackListMapper.selectByPrimaryKey(id);
    }

    @Override
    @Transactional(readOnly = true,propagation = Propagation.SUPPORTS)

    public List<TBlackList> findAll() {
        return tBlackListMapper.selectByExample(null);
    }

    @Override
    public List<TBlackList> findAllByClientId(Long clientId) {
        TBlackListExample example = new TBlackListExample();
        example.createCriteria().andCreaterEqualTo(clientId);//只查询全局的黑名单
        List<TBlackList> tBlackListList = tBlackListMapper.selectByExample(example);//查询到所有的数据
        return tBlackListList;
    }

    @Override
    @Transactional(readOnly = true,propagation = Propagation.SUPPORTS)
    public PageInfo findByPage(QueryDTO queryDTO) {
        PageHelper.offsetPage(queryDTO.getOffset(), queryDTO.getLimit());
        TBlackListExample example = new TBlackListExample();
        String sort = queryDTO.getSort();
        if (!StringUtils.isEmpty(sort)) {
            example.setOrderByClause("id");
        }
        List<TBlackList> tBlackLists = tBlackListMapper.selectByExample(example);
        PageInfo<TBlackList> info = new PageInfo<>(tBlackLists);
        return info;
    }

    @Override
    @Transactional(readOnly = true,propagation = Propagation.SUPPORTS)
    public void sync2RediseBlack() {
        TBlackListExample example = new TBlackListExample();
        example.createCriteria().andOwntypeEqualTo((short) 1);//只查询全局的黑名单
        List<TBlackList> tBlackListList = tBlackListMapper.selectByExample(example);//查询到所有的数据
        //TODO 下面注释的是之前每个黑名单一个key的方式,更改为 一个key对应所有黑名单的方式
//        Map<String, Object> map = tBlackListList.stream().map(tBlackList -> CacheConstants.CACHE_PREFIX_BLACK + tBlackList.getMobile())//
//                .collect(Collectors.toMap(String -> String, String::length));
//
//        cacheService.pipelineOps(map);

        //---------华丽的分割线----------
        //以set当时存取
        //因为同步是将所有数据保存到缓存中,所以为了防止数据错乱,可以先删除这个key,再添加数据
//        cacheService.del(CacheConstants.CACHE_BLACK_KEY);
//        List<String> numList = tBlackListList.stream().map(TBlackList::getMobile).collect(Collectors.toList());
//        cacheService.add2Set(CacheConstants.CACHE_BLACK_KEY, numList);
//        MessageChannel messageChannel = blackCacheChange.updateBlackChannel();
//        messageChannel.send(new GenericMessage<>("0"));//发送消息,消息内容本身没有任何意义,主要的目的是让策略模块收到消息,知道发生黑名单变化了,然后更新缓存

        //操作缓存不应当影响执行结果,所以通过异步执行
        // 通过事件当时发送更新缓存的消息
        context.publishEvent(UpdateBlackEvent.createEvent(EventType.SYNC, tBlackListList));
    }
}

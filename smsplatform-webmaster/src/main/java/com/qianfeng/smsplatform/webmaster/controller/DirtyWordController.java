package com.qianfeng.smsplatform.webmaster.controller;

import com.github.pagehelper.PageInfo;
import com.qianfeng.smsplatform.webmaster.dto.DataGridResult;
import com.qianfeng.smsplatform.common.dto.QueryDTO;
import com.qianfeng.smsplatform.webmaster.pojo.TDirtyword;
import com.qianfeng.smsplatform.webmaster.service.DirtywordService;
import com.qianfeng.smsplatform.common.dto.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@ResponseBody
public class DirtyWordController {

    @Autowired
    private DirtywordService dirtywordService;

    @RequestMapping("/sys/message/list")
    public R findDirtyword(QueryDTO queryDTO){
        PageInfo pageInfo = dirtywordService.findByPage(queryDTO);
        R ok = R.ok();
        ok.put("total", pageInfo.getTotal());
        ok.put("rows", pageInfo.getList());
        return ok;
    }

    @RequestMapping("/sys/message/del")
    public R delDirtyword(@RequestBody List<Long> ids){
        for (Long id : ids) {
            dirtywordService.delDirtyword(id);
        }
        return R.ok();
    }

    @RequestMapping("/sys/message/info/{id}")
    public R findById(@PathVariable("id") Long id){
        TDirtyword tDirtyword = dirtywordService.findById(id);
        return R.ok().put("message",tDirtyword);
    }

    @RequestMapping("/sys/message/save")
    public R addDirtyword(@RequestBody TDirtyword tDirtyword){
        int i = dirtywordService.addDirtyword(tDirtyword);
        return i>0?R.ok():R.error("添加失败");
    }

    @RequestMapping("/sys/message/update")
    public R updateDirtyword(@RequestBody TDirtyword tDirtyword){
        int i = dirtywordService.updateDirtyword(tDirtyword);
        return i>0?R.ok():R.error("修改失败");
    }
    @RequestMapping("/sys/message/sync2redis")
    public R syncDirtyword2Redis(){
        dirtywordService.syncDirtyword2Redis();
        return R.ok();
    }

}

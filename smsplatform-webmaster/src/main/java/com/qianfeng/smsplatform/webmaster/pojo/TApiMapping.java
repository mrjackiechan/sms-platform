package com.qianfeng.smsplatform.webmaster.pojo;

import java.io.Serializable;
import java.util.Date;

public class TApiMapping implements Serializable {
    private Integer id;

    private String gatewayApiName;

    private String serviceId;

    private String insideApiUrl;

    private Date createDate;

    private Date updateTime;

    private Byte state;

    private String description;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGatewayApiName() {
        return gatewayApiName;
    }

    public void setGatewayApiName(String gatewayApiName) {
        this.gatewayApiName = gatewayApiName == null ? null : gatewayApiName.trim();
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId == null ? null : serviceId.trim();
    }

    public String getInsideApiUrl() {
        return insideApiUrl;
    }

    public void setInsideApiUrl(String insideApiUrl) {
        this.insideApiUrl = insideApiUrl == null ? null : insideApiUrl.trim();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Byte getState() {
        return state;
    }

    public void setState(Byte state) {
        this.state = state;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }
}
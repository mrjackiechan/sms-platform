package com.qianfeng.smsplatform.webmaster.service.api;

//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼                  BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  
//  


import com.github.pagehelper.PageInfo;
import com.qianfeng.smsplatform.common.dto.QueryDTO;
import com.qianfeng.smsplatform.common.dto.R;
import com.qianfeng.smsplatform.webmaster.pojo.TSearchParmas;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Jackiechan on 2021/11/14/下午1:11
 *
 * @author Jackiechan
 * @version 1.0
 * @since 1.0
 */
@FeignClient("smsplatform-searchparmas")
@RequestMapping("/searchparams")
public interface SearchParamsFeign {

    @PostMapping("/save")
    R addsearchparams(@RequestBody TSearchParmas tsearchparams);

    @PostMapping("/del")
    R delsearchparamss(@RequestBody List<Integer> id);

    @PostMapping("/update")
    R updatesearchparams(@RequestBody TSearchParmas tsearchparams);


    @GetMapping("/info/{id}")
    TSearchParmas findById(@PathVariable Integer id);

    @GetMapping("/all")
    List<TSearchParmas> findAll();

    @GetMapping("/list")
    PageInfo findByPage(@RequestBody QueryDTO queryDTO);

    @RequestMapping("/state")
    List<TSearchParmas> findByState(@RequestParam(defaultValue = "1")short state);

}

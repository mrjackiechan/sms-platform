package com.qianfeng.smsplatform.webmaster.controller;

//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼                  BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  
//  


import com.github.pagehelper.PageInfo;
import com.qianfeng.smsplatform.common.dto.QueryDTO;
import com.qianfeng.smsplatform.common.dto.R;
import com.qianfeng.smsplatform.webmaster.pojo.TLimit;
import com.qianfeng.smsplatform.webmaster.service.LimitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Administrator on 2021/8/8/18:35
 *
 * @author Administrator
 * @version 1.0
 * @since 1.0
 */
@RestController
public class LimitController {

    private LimitService limitService;

    @Autowired
    public void setLimitService(LimitService limitService) {
        this.limitService = limitService;
    }


    @PostMapping("/sys/limit/save")
    public R addLimit(@RequestBody TLimit tLimit) {

        return limitService.addLimit(tLimit);
    }

    @PostMapping("/sys/limit/del")
    public R delLimits(@RequestBody List<Integer> id) {
        return limitService.delLimits(id);
    }

    @PostMapping("/sys/limit/update")
    public R updateLimit(@RequestBody TLimit tLimit) {
        return limitService.updateLimit(tLimit);
    }


    @GetMapping("/sys/limit/info/{id}")
    public R findById(@PathVariable  Integer id) {
        TLimit limit = limitService.findById(id);
        return R.ok().put("limit",limit);
    }

    @GetMapping("/sys/limit/all")
    public R findAll() {
        List<TLimit> limitList = limitService.findAll();
        return R.ok().put("sites", limitList);
    }

    @GetMapping("/sys/limit/list")
    public R findByPage(QueryDTO queryDTO) {
        PageInfo pageInfo = limitService.findByPage(queryDTO);
        R ok = R.ok();
        ok.put("total", pageInfo.getTotal());
        ok.put("rows", pageInfo.getList());
        return ok;
    }

    @GetMapping("/sys/limit/sync")
    public R syncLimits() {
        limitService.syncLimits();
        return R.ok();
    }
}

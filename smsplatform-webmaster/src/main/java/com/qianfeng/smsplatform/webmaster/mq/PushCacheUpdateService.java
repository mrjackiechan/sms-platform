package com.qianfeng.smsplatform.webmaster.mq;


//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼            BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  


import com.qianfeng.smsplatform.common.constants.RabbitMqConsants;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

/**
 * Created by jackiechan on 2021/8/1 14:59
 *
 * @author jackiechan
 * 人学始知道，不学非自然。
 */

public interface PushCacheUpdateService {
    @Output(RabbitMqConsants.TOPIC_BLACKNUM_UPDATE)//更新黑名单策略
    MessageChannel updateBlackChannel();

    @Output(RabbitMqConsants.TOPIC_DIRTYWORDS_UPDATE)//更新敏感词策略
    MessageChannel updateDirtyWordsChannel();

     @Output(RabbitMqConsants.TOPIC_STRATEGYEXECFILTERS_UPDATE)// 更新策略过滤器策略
     MessageChannel updateStragetyFiltersChannel();

     @Output(RabbitMqConsants.TOPIC_APIGATEWAYEXECFILTERS_UPDATE)// 更新网关过滤器策略
     MessageChannel updateApiGatewayFiltersChannel();

     @Output(RabbitMqConsants.TOPIC_LIMIT_UPDATE)// 更新限流策略
     MessageChannel updateLimitsChannel();

    @Output(RabbitMqConsants.TOPIC_SEARCHPARAMS_UPDATE)// 更新查询参数规则
    MessageChannel updateSearchParamsChannel();

    @Output(RabbitMqConsants.TOPIC_APIMAPPING_UPDATE)// 更新api映射参数规则
    MessageChannel updateApiMappingChannel();

    @Output(RabbitMqConsants.TOPIC_PUBLICPARAMS_UPDATE)// 更新公共参数规则
    MessageChannel updatePublicParamsChannel();


    @Output(RabbitMqConsants.TOPIC_GRAYRELEASE_UPDATE)// 更新灰度发布规则
    MessageChannel updateGrayReleaseChannel();
}

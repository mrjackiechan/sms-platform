package com.qianfeng.smsplatform.webmaster.service;

import com.github.pagehelper.PageInfo;
import com.qianfeng.smsplatform.common.dto.R;
import com.qianfeng.smsplatform.common.dto.QueryDTO;
import com.qianfeng.smsplatform.webmaster.pojo.TSearchParmas;

import java.util.List;

public interface SearchParmasService {

    R updateSearchParmas(TSearchParmas searchParmas);


    R addSearchParmas(TSearchParmas searchParmas);

    R deleteSearchParmas(List<Integer> ids);

    TSearchParmas findById(Integer id);

    List<TSearchParmas> findAll();

    PageInfo findByPage(QueryDTO queryDTO);

    List<TSearchParmas> findByState(Short state);

    void syncSearchParmas();
}
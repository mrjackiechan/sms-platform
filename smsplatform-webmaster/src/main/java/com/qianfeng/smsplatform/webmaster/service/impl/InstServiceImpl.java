package com.qianfeng.smsplatform.webmaster.service.impl;

import com.qianfeng.smsplatform.webmaster.pojo.TInst;
import com.qianfeng.smsplatform.webmaster.service.InstService;
import com.qianfeng.smsplatform.webmaster.service.api.InstFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.List;

@Service
@Transactional
public class InstServiceImpl implements InstService {

    private InstFeign instFeign;

    @Autowired
    public void setInstFeign(InstFeign instFeign) {
        this.instFeign = instFeign;
    }

    @Override
    @Transactional(readOnly = true,propagation = Propagation.SUPPORTS)
    public List<TInst> findProvs() {
        return instFeign.findProvs();
    }

    @Override
    @Transactional(readOnly = true,propagation = Propagation.SUPPORTS)
    public List<TInst> findCitys(Long provId) {
        if (ObjectUtils.isEmpty(provId)) {
            return null;
        }
        return instFeign.findCitys(provId);
    }

    @Override
    public TInst findById(Long provId) {
        if (ObjectUtils.isEmpty(provId)) {
            return null;
        }
        return instFeign.findById(provId);
    }

    @Override
    public List<TInst> findByParentIdAndCityId(Long provId, Long cityId) {
        if (ObjectUtils.isEmpty(provId)||ObjectUtils.isEmpty(cityId)) {
            return null;
        }
        return instFeign.findByParentIdAndCityId(provId, cityId);
    }

}

package com.qianfeng.smsplatform.webmaster.controller;


//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼                  BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  


import com.github.pagehelper.PageInfo;
import com.qianfeng.smsplatform.common.dto.QueryDTO;
import com.qianfeng.smsplatform.common.dto.R;
import com.qianfeng.smsplatform.webmaster.pojo.TApiGateWayFilter;
import com.qianfeng.smsplatform.webmaster.service.ApiGateWayFiltersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by jackiechan on 2021-02-28 09:49
 *
 * @Author jackiechan
 */
@RestController
public class ApiGatewayFiltersController {
    @Autowired
    private ApiGateWayFiltersService apiGateWayFiltersService;
    
    @Autowired
    public void setApiGateWayFiltersService(ApiGateWayFiltersService apiGateWayFiltersService) {
        this.apiGateWayFiltersService = apiGateWayFiltersService;
    }

    /**
     * 修改某个分组的过滤器的顺序
     *
     * @return
     */
    @PostMapping("/sys/apigatewayfilter/update")
    public R updateFilters(@RequestBody TApiGateWayFilter filter) {
        R result = apiGateWayFiltersService.updateFilters(filter);
        return result;
    }


    /**
     * 添加一个过滤器的分组
     *
     * @param filter 过滤器
     * @return
     */
    @PostMapping("/sys/apigatewayfilter/save")
    public R addFilters(@RequestBody TApiGateWayFilter filter) {

        return   apiGateWayFiltersService.addFilters(filter);


    }

    @PostMapping("/sys/apigatewayfilter/del")
    public R deleteFilters(@RequestBody List<Integer> ids) {
        return    apiGateWayFiltersService.deleteFilters(ids);

    }

    @GetMapping("/sys/apigatewayfilter/info/{id}")
    public R findById(@PathVariable Integer id) {
        TApiGateWayFilter filter = apiGateWayFiltersService.findById(id);
        return R.ok().put("filter", filter);
    }

    @GetMapping("/sys/apigatewayfilter/all")
    public R findAll() {
        List<TApiGateWayFilter> filterList = apiGateWayFiltersService.findAll();
        return R.ok().put("sites", filterList);
    }

    @GetMapping("/sys/apigatewayfilter/list")
    public R findByPage(QueryDTO queryDTO) {
        PageInfo pageInfo = apiGateWayFiltersService.findByPage(queryDTO);
        R ok = R.ok();
        ok.put("total", pageInfo.getTotal());
        ok.put("rows", pageInfo.getList());
        return ok;
    }

    /**
     * 同步启用的数据到redis
     * @return
     */
    @GetMapping("/sys/apigatewayfilter/sync")
    public R syncFilters() {
        apiGateWayFiltersService.syncFilters();
        return R.ok();
    }


}

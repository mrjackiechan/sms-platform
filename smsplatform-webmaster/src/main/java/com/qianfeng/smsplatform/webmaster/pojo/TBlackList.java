package com.qianfeng.smsplatform.webmaster.pojo;

import java.io.Serializable;

public class TBlackList implements Serializable {
    private Long id;

    private String mobile;

    private Short owntype;

    private Integer creater;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public Short getOwntype() {
        return owntype;
    }

    public void setOwntype(Short owntype) {
        this.owntype = owntype;
    }

    public Integer getCreater() {
        return creater;
    }

    public void setCreater(Integer creater) {
        this.creater = creater;
    }
}
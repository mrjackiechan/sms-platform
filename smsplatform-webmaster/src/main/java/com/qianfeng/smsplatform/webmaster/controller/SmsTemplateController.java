package com.qianfeng.smsplatform.webmaster.controller;

//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼                  BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  
//  


import com.github.pagehelper.PageInfo;
import com.qianfeng.smsplatform.webmaster.dto.DataGridResult;
import com.qianfeng.smsplatform.common.dto.QueryDTO;
import com.qianfeng.smsplatform.common.dto.R;
import com.qianfeng.smsplatform.webmaster.pojo.TSmsTemplate;
import com.qianfeng.smsplatform.webmaster.service.SmsTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by jackiechan on 2021/8/7/23:09
 *
 * @Author jackiechan
 */
@RestController
public class SmsTemplateController {

    private SmsTemplateService smsTemplateService;

    @Autowired
    public void setSmsTemplateService(SmsTemplateService smsTemplateService) {
        this.smsTemplateService = smsTemplateService;
    }

    @PostMapping("/sys/smstemp/save")
    public R addsmstemp(@RequestBody TSmsTemplate tsmstemp) {
        smsTemplateService.addSmsTemplate(tsmstemp);
        return R.ok();
    }

    @PostMapping("/sys/smstemp/del")
    public R delsmstemp(@RequestBody  List<Integer> ids) {
        smsTemplateService.delSmsTemplate(ids);
        return R.ok();
    }

    @PostMapping("/sys/smstemp/update")
    public R updatesmstemp(@RequestBody TSmsTemplate tsmstemp) {
        smsTemplateService.updateSmsTemplate(tsmstemp);
        return R.ok();
    }

    @GetMapping("/sys/smstemp/info/{id}")
    public R findById(@PathVariable Integer id) {
        TSmsTemplate smsTemplate = smsTemplateService.findById(id);
        return R.ok().put("smstemp",smsTemplate);
    }

    @GetMapping("/sys/smstemp/all")
    public R findAll() {
        List<TSmsTemplate> templateList = smsTemplateService.findAll();
        return R.ok().put("sites",templateList);
    }

    @GetMapping("/sys/smstemp/list")
    public R findByPage(QueryDTO queryDTO) {
        PageInfo pageInfo = smsTemplateService.findByPage(queryDTO);
        R ok = R.ok();
        ok.put("total", pageInfo.getTotal());
        ok.put("rows", pageInfo.getList());
        return ok;
    }
}

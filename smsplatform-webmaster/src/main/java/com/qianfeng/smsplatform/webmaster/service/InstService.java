package com.qianfeng.smsplatform.webmaster.service;

import com.qianfeng.smsplatform.webmaster.pojo.TInst;

import java.util.List;

public interface InstService {
    //查询所有的省
    List<TInst> findProvs();

    //查询省对应的市
    List<TInst> findCitys(Long provId);

    TInst findById(Long provId);

    List<TInst> findByParentIdAndCityId(Long provId, Long cityId);
}

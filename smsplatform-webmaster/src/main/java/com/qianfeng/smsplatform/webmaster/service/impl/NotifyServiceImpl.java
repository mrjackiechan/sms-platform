package com.qianfeng.smsplatform.webmaster.service.impl;


//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼            BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  


import com.github.pagehelper.PageInfo;
import com.qianfeng.smsplatform.common.dto.R;
import com.qianfeng.smsplatform.common.check.CheckType;
import com.qianfeng.smsplatform.common.dto.QueryDTO;
import com.qianfeng.smsplatform.webmaster.events.UpdateNotifyEvent;
import com.qianfeng.smsplatform.common.exceptions.AddDataErrorException;
import com.qianfeng.smsplatform.common.exceptions.DeleteDataErrorException;
import com.qianfeng.smsplatform.common.exceptions.QueryDataErrorException;
import com.qianfeng.smsplatform.common.exceptions.UpdateDataErrorException;
import com.qianfeng.smsplatform.webmaster.pojo.TNotify;
import com.qianfeng.smsplatform.webmaster.service.NotifyService;
import com.qianfeng.smsplatform.webmaster.service.api.NotifyServiceFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by jackiechan on 2021/8/7 14:40
 *
 * @author jackiechan
 * @version 1.0
 * @since 1.0
 */
@Service
@Transactional
public class NotifyServiceImpl implements NotifyService {
    @Autowired
    private ApplicationContext context;

    private NotifyServiceFeignClient notifyServiceFeignClient;

    @Autowired
    public void setNotifyServiceFeignClient(NotifyServiceFeignClient notifyServiceFeignClient) {
        this.notifyServiceFeignClient = notifyServiceFeignClient;
    }

    @Override
    public R addNotify(TNotify tNotify) {
        R r = notifyServiceFeignClient.addNotify(tNotify);
        if (r.getCode() == 0) {
            context.publishEvent(new UpdateNotifyEvent());
        }else{
            throw new AddDataErrorException(r.getMsg(), r.getCode());
        }
        return r;
    }

    @Override
    public R delNotify(List<Integer> ids) {
        if (ids == null || ids.size() <= 0) {
            throw new DeleteDataErrorException("没有传递主键", -1);
        }
        //TODO 更新本地通知缓存数据
        R r = notifyServiceFeignClient.delNotify(ids);
        if (r.getCode() == 0) {
            context.publishEvent(new UpdateNotifyEvent());
        }else{
            throw new DeleteDataErrorException(r.getMsg(), r.getCode());
        }
        return r;
    }

    @Override
    public R updateNotify(TNotify tNotify) {
        if (tNotify.isNull(CheckType.UPDATE)) {
            throw new UpdateDataErrorException("数据不完整,请检查后再试", -1);
        }
        R r = notifyServiceFeignClient.updateNotify(tNotify);
        if (r.getCode() == 0) {
            context.publishEvent(new UpdateNotifyEvent());
        }else{
            throw new UpdateDataErrorException(r.getMsg(), r.getCode());
        }
        return r;
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public TNotify findById(Integer id) {
        if (id == null || id <= 0) {
            throw new QueryDataErrorException("没有传递主键", -1);
        }
        return notifyServiceFeignClient.findById(id);
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<TNotify> findAll() {
        return notifyServiceFeignClient.findAll();
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public PageInfo findByPage(QueryDTO queryDTO) {
        PageInfo info = notifyServiceFeignClient.findByPage(queryDTO);
        return info;
    }
}

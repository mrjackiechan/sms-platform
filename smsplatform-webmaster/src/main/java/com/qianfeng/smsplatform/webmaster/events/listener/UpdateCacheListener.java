package com.qianfeng.smsplatform.webmaster.events.listener;


//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼            BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qianfeng.smsplatform.common.constants.CacheConstants;
import com.qianfeng.smsplatform.webmaster.cache.NotifyCache;
import com.qianfeng.smsplatform.webmaster.events.*;
import com.qianfeng.smsplatform.webmaster.mq.PushCacheUpdateService;
import com.qianfeng.smsplatform.webmaster.pojo.*;
import com.qianfeng.smsplatform.webmaster.service.api.CacheService;
import com.qianfeng.smsplatform.webmaster.util.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by jackiechan on 2021/8/1 11:35
 *
 * @author jackiechan
 * 人学始知道，不学非自然。
 */

@Component
@EnableAsync
public class UpdateCacheListener {
    //获取是否发送更新数据消息的service,内部封装有通知更新的封装对象map
    private NotifyCache notifyCache;
    //缓存服务
    private CacheService cacheService;

    private ObjectMapper objectMapper;

    @Autowired
    public void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Autowired
    public void setNotifyCache(NotifyCache notifyCache) {
        this.notifyCache = notifyCache;
    }

    @Autowired
    public void setCacheService(CacheService cacheService) {
        this.cacheService = cacheService;
    }

    //发送消息的服务
    private PushCacheUpdateService pushCacheUpdateService;

    @Autowired
    public void setPushCacheUpdateService(PushCacheUpdateService pushCacheUpdateService) {
        this.pushCacheUpdateService = pushCacheUpdateService;
    }

    /**
     * 更新黑名单,主要是在增删改查的情况下更新redis并发送消息更新策略模块
     *
     * @param event
     */
    @EventListener
    @Async
    public void onBlackEvent(UpdateBlackEvent event) {
        System.err.println("发送更新黑名单缓存的事件" + Thread.currentThread().getName());
        TNotify tNotify = notifyCache.getNotifyByTag("blacklist");
        if (tNotify==null||tNotify.getCacheState() == 0) {
            //如果不更新缓存,就退出事件,因为不更新缓存,也无法更新其他项目中的缓存
            System.err.println("未开启缓存同步,操作被拦截");
            return;
        }
        switch (event.getEventType()) {
            //添加和更新缓存都是添加一条数据,更新会先执行删除操作
            case ADD:
                cacheService.add2Set(CacheConstants.CACHE_BLACK_KEY, Arrays.asList(event.gettBlackList().get(0).getMobile()));

                break;
            case UPDATE:
                //更新黑名单需要先删除数据,所以可以通过删除来触发消息,更新可以直接return
                cacheService.add2Set(CacheConstants.CACHE_BLACK_KEY, Arrays.asList(event.gettBlackList().get(0).getMobile()));
                return;
            case DELETE:
                cacheService.deleteFromSet(CacheConstants.CACHE_BLACK_KEY, event.gettBlackList().get(0).getMobile());
                break;
            case SYNC:
                cacheService.del(CacheConstants.CACHE_BLACK_KEY);
                List<String> numList = event.gettBlackList().stream().map(TBlackList::getMobile).collect(Collectors.toList());
                cacheService.add2Set(CacheConstants.CACHE_BLACK_KEY, numList);
                break;
        }
        pushMessage(tNotify, pushCacheUpdateService.updateBlackChannel());
    }


    /**
     * 更新敏感词,主要是在增删改查的情况下更新redis并发送消息更新策略模块
     *
     * @param event
     */
    @EventListener
    @Async
    public void onDirtyWordsEvent(UpdateDirtyWordsEvent event) {
        System.err.println("发送更新敏感词缓存的事件" + Thread.currentThread().getName());
        TNotify tNotify = notifyCache.getNotifyByTag("ditryword");
        if (tNotify==null||tNotify.getCacheState() == 0) {
            //如果不更新缓存,就退出事件,因为不更新缓存,也无法更新其他项目中的缓存
            System.err.println("未开启缓存同步,操作被拦截");
            return;
        }
        switch (event.getEventType()) {
            case ADD:
                cacheService.add2Set(CacheConstants.CACHE_DIRTY_KEY, Arrays.asList(event.getTDirtywordList().get(0).getDirtyword()));
            case UPDATE:
                //更新敏感词会删除数据,所以通过删除来发送消息即可,更新直接return
                cacheService.add2Set(CacheConstants.CACHE_DIRTY_KEY, Arrays.asList(event.getTDirtywordList().get(0).getDirtyword()));
                return;

            case DELETE:
                cacheService.deleteFromSet(CacheConstants.CACHE_DIRTY_KEY, event.getTDirtywordList().get(0).getDirtyword());
                break;

            case SYNC:
                cacheService.del(CacheConstants.CACHE_DIRTY_KEY);
                List<String> numList = event.getTDirtywordList().stream().map(TDirtyword::getDirtyword).collect(Collectors.toList());
                cacheService.add2Set(CacheConstants.CACHE_DIRTY_KEY, numList);
                break;
        }

            pushMessage(tNotify, pushCacheUpdateService.updateDirtyWordsChannel());

    }

    /**
     * 更新号段区域数据到redis
     *
     * @param event
     */
    @EventListener
    @Async
    public void onPhaseUpDate(UpdatePhaseEvent event) {
        List<TPhase> phaseList = event.getPhaseList();
        switch (event.getEventType()) {

            case ADD:
            case UPDATE:
                TPhase tPhase = phaseList.get(0);
                cacheService.saveCache(CacheConstants.CACHE_PREFIX_PHASE + tPhase.getPhase(), tPhase.getProvId() + "&" + tPhase.getCityId());
                break;
            case DELETE:
                cacheService.del(CacheConstants.CACHE_PREFIX_PHASE  + phaseList.get(0).getPhase());
                break;
            case SYNC:
                List<TPhase> allLists = event.getPhaseList();
                //将数据转成 map 直接发送到缓存服务,批量添加到缓存
                Map<String, Object> map = allLists.stream().collect(HashMap::new, (m, currenttPhase) -> m.put(CacheConstants.CACHE_PREFIX_PHASE + currenttPhase.getPhase(), currenttPhase.getProvId().toString() + "&" + currenttPhase.getCityId().toString()), HashMap::putAll);
                cacheService.mSet(map);
                break;
        }
    }

    @EventListener
    @Async
    public void onClientBusinessUpdate(UpdateClientBusinessEvent event) {
        switch (event.getEventType()) {

            case ADD:
            case UPDATE:
                TClientBusiness clientBusiness = event.gettClientBusinessList().get(0);
                //转成 map 后放到 hash 中保存
                Map<String, String> map1 = JsonUtil.objectToMap(clientBusiness);
                cacheService.hmset(CacheConstants.CACHE_PREFIX_CLIENT + clientBusiness.getId(), map1);
                break;
            case DELETE:
                cacheService.del(CacheConstants.CACHE_PREFIX_CLIENT + event.gettClientBusinessList().get(0).getId());
                break;
            case SYNC:
                break;
        }
    }

    @Async
    @EventListener
    public void onEvent(UpdateStrageFilterEvent event) {
        System.err.println("收到更新策略过滤器列表的事件");
        TNotify tNotify = notifyCache.getNotifyByTag("stragetyfilterlist");
        if (tNotify==null||tNotify.getCacheState() == 0) {
            //如果不更新缓存,就退出事件,因为不更新缓存,也无法更新其他项目中的缓存
            System.err.println("未开启缓存同步,操作被拦截");
            return;
        }
        //先移除原有的过滤器
        cacheService.del(CacheConstants.CACHE_STRAGETY_FILTER_KEY);
        //添加新的过滤器,向右添加来保证数据
        cacheService.rPush(CacheConstants.CACHE_STRAGETY_FILTER_KEY, event.getFilters().split(","));
        //发送mq消息,通知策略模块更新策略
        pushMessage(tNotify, pushCacheUpdateService.updateStragetyFiltersChannel());

    }

    @Async
    @EventListener
    public void onEvent(UpdateApiGatewayFilterEvent event) {
        System.err.println("收到更新API网关过滤器列表的事件");
        TNotify tNotify = notifyCache.getNotifyByTag("apigatewayfilterlist");
        if (tNotify==null||tNotify.getCacheState() == 0) {
            //如果不更新缓存,就退出事件,因为不更新缓存,也无法更新其他项目中的缓存
            System.err.println("未开启缓存同步,操作被拦截");
            return;
        }
        //先移除原有的过滤器
        cacheService.del(CacheConstants.CACHE_APIGATEWAY_FILTER_KEY);
        //添加新的过滤器,向右添加来保证数据
        cacheService.rPush(CacheConstants.CACHE_APIGATEWAY_FILTER_KEY, event.getFilters().split(","));
        //发送mq消息,通知策略模块更新策略
        pushMessage(tNotify, pushCacheUpdateService.updateApiGatewayFiltersChannel());

    }

    @Async
    @EventListener
    public void onEvent(UpdateLimitEvent event) {
        System.err.println("收到限流策略更新的事件");
        TNotify tNotify = notifyCache.getNotifyByTag("limit");
        if (tNotify==null||tNotify.getCacheState() == 0) {
            //如果不更新缓存,就退出事件,因为不更新缓存,也无法更新其他项目中的缓存
            System.err.println("未开启缓存同步,操作被拦截");
            return;
        }
        //先移除原有的过滤器
        cacheService.del(CacheConstants.CACHE_LIMITSTRATEGY_KEY);
        //添加新的过滤器,向右添加来保证数据
        event.getLimitList().forEach(limit -> {
            cacheService.add2Zset(CacheConstants.CACHE_LIMITSTRATEGY_KEY, limit.getLimitCount() + "", limit.getLimitTime());
        });

        //发送mq消息,通知策略模块更新策略
        pushMessage(tNotify, pushCacheUpdateService.updateLimitsChannel());

    }



    @Async
    @EventListener
    public void onEvent(SearchParmasEvent event) {
        System.err.println("收到搜索条件变化的事件");
        TNotify tNotify = notifyCache.getNotifyByTag("searchparmas");
        if (tNotify==null||tNotify.getCacheState() == 0) {
            //如果不更新缓存,就退出事件,因为不更新缓存,也无法更新其他项目中的缓存
            System.err.println("未开启缓存同步,操作被拦截");
            return;
        }
        //先移除原有的过滤器
        cacheService.del(CacheConstants.CACHE_SEARCHPARAM_KEY);
        //添加新的过滤器,向右添加来保证数据
        Map<String, TSearchParmas> parmasMap = event.getParmasList().stream().collect(Collectors.toMap(TSearchParmas::getName, TSearchParmas -> TSearchParmas));

        try {
            cacheService.saveCache(CacheConstants.CACHE_SEARCHPARAM_KEY, objectMapper.writeValueAsString(parmasMap));        //发送mq消息,通知策略模块更新策略
            pushMessage(tNotify, pushCacheUpdateService.updateSearchParamsChannel());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }


    }
    @Async
    @EventListener
    public void onEvent(UpdateApiMappingEvent event) {
        System.err.println("收到API网关变化的事件");
        TNotify tNotify = notifyCache.getNotifyByTag("apimapping");
        if (tNotify==null||tNotify.getCacheState() == 0) {
            //如果不更新缓存,就退出事件,因为不更新缓存,也无法更新其他项目中的缓存
            System.err.println("未开启缓存同步,操作被拦截");
            return;
        }
        cacheService.del(CacheConstants.CACHE_APIMAPPING_KEY);
        List<TApiMapping> mappings = event.getMappings();
        cacheService.save2Cache(CacheConstants.CACHE_APIMAPPING_KEY,mappings);
        pushMessage(tNotify, pushCacheUpdateService.updateApiMappingChannel());
    }

    @Async
    @EventListener
    public void onEvent(UpdatePublicParamsEvent event) {
        System.err.println("收到API网关参数变化的事件");
        TNotify tNotify = notifyCache.getNotifyByTag("publicrapams");
        if (tNotify==null||tNotify.getCacheState() == 0) {
            //如果不更新缓存,就退出事件,因为不更新缓存,也无法更新其他项目中的缓存
            System.err.println("未开启缓存同步,操作被拦截");
            return;
        }
        cacheService.del(CacheConstants.CACHE_PUBLICPARAMS_KEY);
        List<TPublicParams> publicParams = event.getPublicParams();
        cacheService.save2Cache(CacheConstants.CACHE_PUBLICPARAMS_KEY,publicParams);
        pushMessage(tNotify, pushCacheUpdateService.updatePublicParamsChannel());
    }
    @Async
    @EventListener
    public void onEvent(UpdateGrayReleaseEvent event) {
        System.err.println("收到API网关变化的事件");
        TNotify tNotify = notifyCache.getNotifyByTag("grayreleaseconfig");
        if (tNotify==null||tNotify.getCacheState() == 0) {
            //如果不更新缓存,就退出事件,因为不更新缓存,也无法更新其他项目中的缓存
            System.err.println("未开启缓存同步,操作被拦截");
            return;
        }
        cacheService.del(CacheConstants.CACHE_GRAYRELEASE_KEY);
        List<TGrayReleaseConfig> configList = event.getConfigList();
        cacheService.save2Cache(CacheConstants.CACHE_GRAYRELEASE_KEY,configList);
        pushMessage(tNotify, pushCacheUpdateService.updateGrayReleaseChannel());
    }


    /**
     * 发送消息更新数据
     *
     * @param tNotify
     * @param messageChannel
     */
    private void pushMessage(TNotify tNotify, MessageChannel messageChannel) {
        //获取当前操作是否通过mq刷新数据
     //   TNotify tNotify = notifyCache.getNotifyByTag(notifyTag);
        //如果当前操作有通知对象并且是启用了发送更新通知
        if (tNotify != null && 1 == tNotify.getNotifyState()) {
            System.err.println("发送" + tNotify.getTag() + "消息更新数据");
            //发送mq通知策略更新本地缓存数据
            messageChannel.send(new GenericMessage<>("0"));//发送消息,消息内容本身没有任何意义,主要的目的是让策略模块收到消息,知道发生黑名单变化了,然后更新缓存
        }
    }


}

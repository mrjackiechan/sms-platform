package com.qianfeng.smsplatform.webmaster.pojo;

import com.qianfeng.smsplatform.common.check.CheckNulll;
import com.qianfeng.smsplatform.common.check.CheckType;
import org.springframework.util.StringUtils;

import java.io.Serializable;

public class TSmsTemplate implements Serializable,CheckNulll {
    private Integer id;

    private String paramter;

    private Short owntype;

    private Integer creater;

    private Short status;

    private String template;

    private static final long serialVersionUID = 1L;

    @Override
    public boolean isNull(CheckType type) {
        switch (type) {
            case ADD:
                return StringUtils.isEmpty(template)||StringUtils.isEmpty(paramter);
            case UPDATE:

                return StringUtils.isEmpty(template)&&StringUtils.isEmpty(paramter)&&StringUtils.isEmpty(status);
        }
        return CheckNulll.super.isNull(type);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getParamter() {
        return paramter;
    }

    public void setParamter(String paramter) {
        this.paramter = paramter == null ? null : paramter.trim();
    }

    public Short getOwntype() {
        return owntype;
    }

    public void setOwntype(Short owntype) {
        this.owntype = owntype;
    }

    public Integer getCreater() {
        return creater;
    }

    public void setCreater(Integer creater) {
        this.creater = creater;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template == null ? null : template.trim();
    }
}
package com.qianfeng.smsplatform.webmaster.service.impl;

import com.github.pagehelper.PageInfo;
import com.qianfeng.smsplatform.common.constants.InterfaceExceptionDict;
import com.qianfeng.smsplatform.common.dto.R;
import com.qianfeng.smsplatform.webmaster.events.UpdateApiMappingEvent;
import com.qianfeng.smsplatform.webmaster.pojo.TApiMapping;
import com.qianfeng.smsplatform.webmaster.service.ApiMappingService;
import com.qianfeng.smsplatform.webmaster.service.api.ApiMappinFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ApiMappingServiceImpl implements ApiMappingService {

    private ApplicationContext context;

    @Autowired
    public void setContext(ApplicationContext context) {
        this.context = context;
    }

    private ApiMappinFeign apiMappinFeign;

    @Autowired
    public void setApiMappinFeign(ApiMappinFeign apiMappinFeign) {
        this.apiMappinFeign = apiMappinFeign;
    }

    @Override
    public void addApiMapping(TApiMapping mapping) {
        R add = apiMappinFeign.add(mapping);
        if (mapping.getState() == 1 && add.getCode() == InterfaceExceptionDict.RETURN_STATUS_SUCCESS) {
            syncCache();
        }
    }

    @Override
    public void updateApiMapping(TApiMapping mapping) {
        R update = apiMappinFeign.update(mapping);
        if (update.getCode() == InterfaceExceptionDict.RETURN_STATUS_SUCCESS) {
            syncCache();
        }
    }

    @Override
    public PageInfo getMappingList(Map criteria, int offset, int limit) {

        return apiMappinFeign.table(criteria,offset,limit);
    }

    @Override
    public TApiMapping getMappingById(Integer id) {
        return apiMappinFeign.info(id);
    }

    @Override
    public void deleteMapping(Integer[] ids) {
        if (ids == null || ids.length == 0) {
            return;
        }
        apiMappinFeign.delete(ids);
        syncCache();
    }
    @Override
    public void syncCache() {
        //获取所有数据
        List<TApiMapping> mappingList = apiMappinFeign.findAll((byte) 1);
        context.publishEvent(new UpdateApiMappingEvent(mappingList));
    }
}

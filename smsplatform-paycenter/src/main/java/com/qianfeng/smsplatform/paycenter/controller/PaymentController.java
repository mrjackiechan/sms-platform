package com.qianfeng.smsplatform.paycenter.controller;


//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼            BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  


import com.qianfeng.smsplatform.paycenter.utils.PayCommonUtil;
import com.qianfeng.smsplatform.paycenter.utils.ZxingUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.UUID;

/**
 * Created by jackiechan on 2021/7/25 11:24
 *
 * @author jackiechan
 * 人学始知道，不学非自然。
 */
@RequestMapping("/payment")
@Controller
public class PaymentController {

    @PostMapping("/create")
    //@ResponseBody
    public String createOrder(String body, HttpServletRequest request) throws Exception {
        String order_id = UUID.randomUUID().toString().replaceAll("-", "");
        String url = PayCommonUtil.weixin_pay("1", body, order_id);
       // BufferedImage image = ZxingUtil.createImage(url, 300, 300);
       // ImageIO.write(image,"jpg",response.getOutputStream());
        String realPath = request.getServletContext().getRealPath("/pics");
        File file = new File(realPath);
        if (!file.exists()) {
            file.mkdirs();
        }

        ZxingUtil.encode(url,"jpg",300,300,realPath+"/"+order_id+".jpg");
        request.setAttribute("orderid", order_id);
        request.setAttribute("picurl", "/wxpay/pics/"+order_id+".jpg");
        return "/pay.html";
    }

    @RequestMapping("/createpayment")
    public String createOrder1(String body,String price,String orderid,HttpServletRequest request) throws Exception {
       // String order_id = UUID.randomUUID().toString().replaceAll("-", "");
        String url = PayCommonUtil.weixin_pay(price, body, orderid);
        BufferedImage image = ZxingUtil.createImage(url, 300, 300);
//        ImageIO.write(image,"jpg",response.getOutputStream());
        String realPath = request.getServletContext().getRealPath("/pics");
        File file = new File(realPath);
        if (!file.exists()) {
            file.mkdirs();
        }

        ZxingUtil.encode(url,"jpg",300,300,realPath+"/"+orderid+".jpg");
        request.setAttribute("orderid", orderid);
        request.setAttribute("picurl", "/wxpay/pics/"+orderid+".jpg");
        return "/pay.html";
    }
}

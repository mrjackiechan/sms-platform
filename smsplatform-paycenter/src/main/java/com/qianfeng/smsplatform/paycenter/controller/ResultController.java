package com.qianfeng.smsplatform.paycenter.controller;


//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼            BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  


import com.fasterxml.jackson.databind.ObjectMapper;
import com.qianfeng.smsplatform.paycenter.mq.SendPayResultChannel;
import com.qianfeng.smsplatform.paycenter.utils.PayCommonUtil;
import com.qianfeng.smsplatform.paycenter.utils.PayConfigUtil;
import com.qianfeng.smsplatform.paycenter.utils.XMLUtil;
import org.jdom.JDOMException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by jackiechan on 2021/7/25 15:45
 *
 * @author jackiechan
 * 人学始知道，不学非自然。
 */
@RestController
@RequestMapping("/pay")
public class ResultController {

    private SendPayResultChannel sendPayResultChannel;


    private ObjectMapper objectMapper;
    @Autowired
    public void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Autowired
    public void setSendPayResultChannel(SendPayResultChannel sendPayResultChannel) {
        this.sendPayResultChannel = sendPayResultChannel;
    }

    @RequestMapping("/payresult")
    public String onResult(@RequestBody String xml, HttpServletRequest request) {
       // System.err.println("收到的xml数据是:===>" + xml);
        String realPath = request.getServletContext().getRealPath("/files");
        File file = new File(realPath);
        if (!file.exists()) {
            file.mkdirs();
        }
        try {
            FileOutputStream fos = new FileOutputStream(realPath + "/111.txt");
            fos.write(xml.getBytes(StandardCharsets.UTF_8));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            Map map = XMLUtil.doXMLParse(xml);//将微信的返回结果转成map
            TreeMap treeMap = new TreeMap();
            treeMap.putAll(map);//转成了有序的map

         //   String sign = (String) map.ismove("sign");//因为签名不能参数签名的计算,所以先移除
            if (PayCommonUtil.isTenpaySign("UTF-8",treeMap, PayConfigUtil.API_KEY)) {
                //意味着签名校验程
                //执行自己的业务
                System.err.println("校验数据成功,执行自己的业务结果");

                //发送一个mq消息+
                MessageChannel messageChannel = sendPayResultChannel.sendPayResultChannel();
                treeMap.put("paymentid", 1);//设置支付方式为微信支付
                String json = objectMapper.writeValueAsString(treeMap);
                messageChannel.send(new GenericMessage<>(json));//将订单信息发出去
                //返回结果告诉微信
                return "<xml>\n" +
                        "  <return_code><![CDATA[SUCCESS]]></return_code>\n" +
                        "  <return_msg><![CDATA[OK]]></return_msg>\n" +
                        "</xml>";
            }
        } catch (JDOMException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.err.println("校验失败,有人伪造请求");
        return "<xml>\n" +
                "  <return_code><![CDATA[fsdf]]></return_code>\n" +
                "  <return_msg><![CDATA[fdsfgs]]></return_msg>\n" +
                "</xml>";
    }
}

package com.qianfeng.smsplatform.paycenter.utils;


//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼            BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  


/**
 * Created by jackiechan on 2021/7/25 11:26
 *
 * @author jackiechan
 * 人学始知道，不学非自然。
 */

public class PayConfigUtil {
    public static String APP_ID = "wx632c8f211f8122c6";//我们的微信公众号 id
    public static String MCH_ID = "1497984412";//我们的商户 id
    public static String API_KEY = "sbNCm1JnevqI36LrEaxFwcaT0hkGxFnC";//我们的 API_KEY 用于生成签名
    public static String UFDOOER_URL = "https://api.mch.weixin.qq.com/pay/unifiedorder";//微信统一下单地址
    public static String NOTIFY_URL = "https://securit.qfjava.cn/wxpay/pay/payresult";//我们的回调地址,用于接收微信告诉我们的支付结果
    public static String CREATE_IP = "114.242.26.51"; //发起请求的地址,可以写我们的服务器地址,也可以传递客户的 ip
}

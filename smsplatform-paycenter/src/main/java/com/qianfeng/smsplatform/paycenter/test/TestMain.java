package com.qianfeng.smsplatform.paycenter.test;


//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼            BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  


import org.springframework.util.DigestUtils;

import java.nio.charset.StandardCharsets;
import java.util.TreeMap;

/**
 * Created by jackiechan on 2021/7/25 10:51
 *
 * @author jackiechan
 * 人学始知道，不学非自然。
 */

public class TestMain {

    public static void main(String[] args) {
        TreeMap treeMap = new TreeMap();
        treeMap.put("appid", "wx632c8f211f8122c6");//公众号id
        treeMap.put("mch_id", "1497984412");//商户id
        treeMap.put("nonce_str", "fsdfsfsdsdfs");//公众号id
        treeMap.put("body", "颈椎康复指南");//商品的简单描述
        treeMap.put("out_trade_no", "fsdfsdfsdfdsfsd");//我们的订单id,不能太长
        treeMap.put("total_fee", "10000");//金额
        treeMap.put("spbill_create_ip", "8.8.8.8");//客户的ip
        treeMap.put("notify_url", "http://payment.qfjava.cn");//公众号id
        treeMap.put("trade_type", "NATIVE");//公众号id
        StringBuffer stringBuffer = new StringBuffer();
        treeMap.forEach((key,value)->{
            stringBuffer.append(key);
            stringBuffer.append("=");
            stringBuffer.append(value);
            stringBuffer.append("&");
        });
       // stringBuffer.deleteCharAt(stringBuffer.length() - 1);
        stringBuffer.append("key=sbNCm1JnevqI36LrEaxFwcaT0hkGxFnC");
//        System.err.println(treeMap);
     //   System.err.println(stringBuffer);
        String MD5 = DigestUtils.md5DigestAsHex(stringBuffer.toString().getBytes(StandardCharsets.UTF_8)).toUpperCase();
       // System.err.println(MD5);
        treeMap.put("sign", MD5);

        StringBuffer stringBuffer1 = new StringBuffer();
        stringBuffer1.append("<xml>");
        treeMap.forEach((key,value)->{
            stringBuffer1.append("<");
            stringBuffer1.append(key);
            stringBuffer1.append(">");
            stringBuffer1.append(value);
            stringBuffer1.append("</");
            stringBuffer1.append(key);
            stringBuffer1.append(">");

        });
        stringBuffer1.append(("</xml>"));
        System.err.println(stringBuffer1);

    }

}

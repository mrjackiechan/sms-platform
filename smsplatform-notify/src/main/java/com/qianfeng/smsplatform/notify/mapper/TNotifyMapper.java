package com.qianfeng.smsplatform.notify.mapper;

import com.qianfeng.smsplatform.notify.bean.TNotify;
import com.qianfeng.smsplatform.notify.bean.TNotifyExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TNotifyMapper {
    long countByExample(TNotifyExample example);

    int deleteByExample(TNotifyExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(TNotify record);

    int insertSelective(TNotify record);

    List<TNotify> selectByExample(TNotifyExample example);

    TNotify selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") TNotify record, @Param("example") TNotifyExample example);

    int updateByExample(@Param("record") TNotify record, @Param("example") TNotifyExample example);

    int updateByPrimaryKeySelective(TNotify record);

    int updateByPrimaryKey(TNotify record);
}
package com.qianfeng.smsplatform.notify.controller;

//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼                  BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  
//  


import com.github.pagehelper.PageInfo;
import com.qianfeng.smsplatform.common.dto.QueryDTO;
import com.qianfeng.smsplatform.common.dto.R;
import com.qianfeng.smsplatform.notify.bean.TNotify;
import com.qianfeng.smsplatform.notify.service.NotifyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by jackiechan on 2021/8/7/23:09
 *
 * @Author jackiechan
 */
@RestController
@RequestMapping("/notify")
public class NotifyController {

    private NotifyService tNotifyService;

    @Autowired
    public void settNotifyService(NotifyService tNotifyService) {
        this.tNotifyService = tNotifyService;
    }

    @PostMapping("/save")
    public R addNotify(@RequestBody TNotify tNotify) {
        tNotifyService.addNotify(tNotify);
        return R.ok();
    }

    @PostMapping("/del")
    public R delNotify(@RequestBody  List<Integer> ids) {
        tNotifyService.delNotify(ids);
        return R.ok();
    }

    @PostMapping("/update")
    public R updateNotify(@RequestBody TNotify tNotify) {
        tNotifyService.updateNotify(tNotify);
        return R.ok();
    }

    @GetMapping("/info/{id}")
    public TNotify findById(@PathVariable Integer id) {
        TNotify tNotify = tNotifyService.findById(id);
     //   return R.ok().put("data",tNotify);
        return tNotify;
    }

    @GetMapping("/all")
    public List<TNotify> findAll() {
        List<TNotify> notifyList = tNotifyService.findAll();
     //   return R.ok().put("data",notifyList);
        return notifyList;
    }

    @PostMapping("/list")
    public PageInfo findByPage(@RequestBody QueryDTO queryDTO) {
        PageInfo pageInfo = tNotifyService.findByPage(queryDTO);
        //return R.ok(pageInfo);
        return pageInfo;
    }
}

package com.qianfeng.smsplatform.notify.service.impl;


//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼            BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qianfeng.smsplatform.common.check.CheckType;
import com.qianfeng.smsplatform.common.dto.QueryDTO;
import com.qianfeng.smsplatform.common.exceptions.AddDataErrorException;
import com.qianfeng.smsplatform.common.exceptions.DeleteDataErrorException;
import com.qianfeng.smsplatform.common.exceptions.QueryDataErrorException;
import com.qianfeng.smsplatform.common.exceptions.UpdateDataErrorException;
import com.qianfeng.smsplatform.notify.bean.TNotify;
import com.qianfeng.smsplatform.notify.bean.TNotifyExample;
import com.qianfeng.smsplatform.notify.mapper.TNotifyMapper;
import com.qianfeng.smsplatform.notify.service.NotifyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * Created by jackiechan on 2021/8/7 14:40
 *
 * @author jackiechan
 * @version 1.0
 * @since 1.0
 */
@Service
@Transactional
public class NotifyServiceImpl implements NotifyService {

    private TNotifyMapper notifyMapper;
    @Autowired
    public void setNotifyMapper(TNotifyMapper notifyMapper) {
        this.notifyMapper = notifyMapper;
    }

    @Override
    public int addNotify(TNotify tNotify) {
        if (tNotify.isNull(CheckType.ADD)) {
            throw new AddDataErrorException("数据不完整,请检查后再试", -1);
        }
        int result = notifyMapper.insert(tNotify);
        return result;
    }

    @Override
    public int delNotify(List<Integer> ids) {
        if (ids == null || ids .size()<=0) {
            throw new DeleteDataErrorException("没有传递主键", -1);
        }
        TNotifyExample example = new TNotifyExample();
        example.createCriteria().andIdIn(ids);
        int result = notifyMapper.deleteByExample(example);
        return result;
    }

    @Override
    public int updateNotify(TNotify tNotify) {
        if (tNotify.isNull(CheckType.UPDATE)) {
            throw new UpdateDataErrorException("数据不完整,请检查后再试", -1);
        }
        notifyMapper.updateByPrimaryKeySelective(tNotify);
        return 0;
    }

    @Override
    @Transactional(readOnly = true,propagation = Propagation.SUPPORTS)
    public TNotify findById(Integer id) {
        if (id == null || id <= 0) {
            throw new QueryDataErrorException("没有传递主键", -1);
        }
        return notifyMapper.selectByPrimaryKey(id);
    }

    @Override
    @Transactional(readOnly = true,propagation = Propagation.SUPPORTS)
    public List<TNotify> findAll() {
        return notifyMapper.selectByExample(new TNotifyExample());
    }

    @Override
    @Transactional(readOnly = true,propagation = Propagation.SUPPORTS)
    public PageInfo findByPage(QueryDTO queryDTO) {
        PageHelper.offsetPage(queryDTO.getOffset(), queryDTO.getLimit());
        TNotifyExample example = new TNotifyExample();
        String sort = queryDTO.getSort();
        if (!StringUtils.isEmpty(sort)) {
            example.setOrderByClause("id");
        }
        List<TNotify> tNotifyList = notifyMapper.selectByExample(example);
        PageInfo<TNotify> info = new PageInfo<>(tNotifyList);
        return info;
    }
}

package com.qianfeng.smsplatform.notify.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.qianfeng.smsplatform.common.check.CheckNulll;
import com.qianfeng.smsplatform.common.check.CheckType;
import org.springframework.util.ObjectUtils;

import java.io.Serializable;

public class TNotify implements Serializable, CheckNulll {
    private Integer id;

    private String tag;

    private String desp;

    private Short notifyState;

    private Short cacheState;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag == null ? null : tag.trim();
    }

    public String getDesp() {
        return desp;
    }

    public void setDesp(String desp) {
        this.desp = desp == null ? null : desp.trim();
    }

    public Short getNotifyState() {
        return notifyState;
    }

    public void setNotifyState(Short notifyState) {
        this.notifyState = notifyState;
    }

    public Short getCacheState() {
        return cacheState;
    }

    public void setCacheState(Short cacheState) {
        this.cacheState = cacheState;
    }

    @Override
    @JsonIgnore
    public boolean isNull(CheckType type) {
        switch (type) {
            case ADD:
                return ObjectUtils.isEmpty(tag) || notifyState == null||cacheState==null;
            case UPDATE:
                return ObjectUtils.isEmpty(id) || (ObjectUtils.isEmpty(tag) && ObjectUtils.isEmpty(desp) && notifyState == null&&cacheState==null);
            case DELETE:
                return ObjectUtils.isEmpty(id);
        }
        return CheckNulll.super.isNull(type);
    }
}
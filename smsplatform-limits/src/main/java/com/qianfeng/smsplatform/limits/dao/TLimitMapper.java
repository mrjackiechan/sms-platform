package com.qianfeng.smsplatform.limits.dao;

import com.qianfeng.smsplatform.limits.bean.TLimit;
import com.qianfeng.smsplatform.limits.bean.TLimitExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TLimitMapper {
    long countByExample(TLimitExample example);

    int deleteByExample(TLimitExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(TLimit record);

    int insertSelective(TLimit record);

    List<TLimit> selectByExample(TLimitExample example);

    TLimit selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") TLimit record, @Param("example") TLimitExample example);

    int updateByExample(@Param("record") TLimit record, @Param("example") TLimitExample example);

    int updateByPrimaryKeySelective(TLimit record);

    int updateByPrimaryKey(TLimit record);
}
package com.qianfeng.smsplatform.limits.controller;

//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼                  BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  
//  


import com.github.pagehelper.PageInfo;
import com.qianfeng.smsplatform.common.dto.QueryDTO;
import com.qianfeng.smsplatform.common.dto.R;
import com.qianfeng.smsplatform.limits.bean.TLimit;
import com.qianfeng.smsplatform.limits.service.LimitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Administrator on 2021/8/8/18:35
 *
 * @author Administrator
 * @version 1.0
 * @since 1.0
 */
@RestController
@RequestMapping("/limits")
public class LimitController {

    private LimitService limitService;

    @Autowired
    public void setLimitService(LimitService limitService) {
        this.limitService = limitService;
    }


    @PostMapping("/save")
    public R addLimit(@RequestBody TLimit tLimit) {

        limitService.addLimit(tLimit);
        return R.ok();
    }

    @PostMapping("/del")
    public R delLimits(@RequestBody List<Integer> id) {
        limitService.delLimits(id);
        return R.ok();
    }

    @PostMapping("/update")
    public R updateLimit(@RequestBody TLimit tLimit) {
        limitService.updateLimit(tLimit);
        return R.ok();
    }


    @GetMapping("/info/{id}")
    public TLimit findById(@PathVariable  Integer id) {
        TLimit limit = limitService.findById(id);
        return limit;
    }

    @GetMapping("/all")
    public  List<TLimit> findAll() {
        List<TLimit> limitList = limitService.findAll();
        return limitList;
    }

    @PostMapping("/list")
    public PageInfo findByPage(@RequestBody QueryDTO queryDTO) {
        PageInfo pageInfo = limitService.findByPage(queryDTO);
        return pageInfo;
    }

    @GetMapping("/state")
    List<TLimit> findByState(@RequestParam(defaultValue = "1") short state) {
        return limitService.findByState(state);
    }

    @GetMapping("/stateandtime")
    List<TLimit> findByStateAndTime(@RequestParam(defaultValue = "1") short state,@RequestParam(defaultValue = "-1") int limitTime){
        return limitService.findByStateAndTime(state, limitTime);
    }
}

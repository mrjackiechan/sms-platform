package com.qianfeng.smsplatform.common.constants;

public class StrategyConstants {

    /**黑名单	black
    敏感词	dirtyWords
    限流规则出错	limit
    计费	fee
    路由	router
     **/
    public final static int CHINA_TELCOM = 0; //中国电信
    public final static int CHINA_UNICOM = 1; //中国电信
    public final static int CHINA_MOBILE = 2; //中国电信

}

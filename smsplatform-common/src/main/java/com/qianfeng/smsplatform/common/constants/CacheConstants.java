package com.qianfeng.smsplatform.common.constants;

public class CacheConstants {
    
    public final static String CACHE_PREFIX_CLIENT =PrefixInterface.CACHE_PREFIX+"CLIENT:";
    public final static String CACHE_PREFIX_PHASE = "PHASE:";
    public final static String CACHE_PREFIX_BLACK =PrefixInterface.CACHE_PREFIX+ "BLACK:";
    public final static String CACHE_PREFIX_ROUTER = PrefixInterface.CACHE_PREFIX+"ROUTER:";
    public final static String CACHE_PREFIX_SMS_LIMIT = PrefixInterface.CACHE_PREFIX+"LIMIT:";
    public final static String CACHE_BLACK_KEY = PrefixInterface.CACHE_PREFIX+"BLACKNUM";//通过set保存所有黑名单的时候的key
    public final static String CACHE_DIRTY_KEY = PrefixInterface.CACHE_PREFIX+"DIRTYWORDS";//通过set保存所有敏感词的时候的key
    public final static String CACHE_STRAGETY_FILTER_KEY = PrefixInterface.CACHE_PREFIX+"STRATEGYFILTERS";//通过set保存所有要启用的过滤器
    public final static String CACHE_APIGATEWAY_FILTER_KEY = PrefixInterface.CACHE_PREFIX+"APIGATEWAYFILTERS";//通过set保存所有要启用的过滤器
    public final static String CACHE_LIMITSTRATEGY_KEY =PrefixInterface.CACHE_PREFIX+ "LIMITSTRATEGY";//通过Zset保存所有的限流策略
    public final static String CACHE_SEARCHPARAM_KEY =PrefixInterface.CACHE_PREFIX+ "SEARCHPARAMS";//通过Zset保存所有的限流策略
    public final static String CACHE_APIMAPPING_KEY =PrefixInterface.CACHE_PREFIX+ "APIMAPPING";//保存所有api映射
    public final static String CACHE_PUBLICPARAMS_KEY =PrefixInterface.CACHE_PREFIX+ "PUBLICPARAMS";//保存所有公共参数映射
    public final static String CACHE_GRAYRELEASE_KEY =PrefixInterface.CACHE_PREFIX+ "GRAYRELEASE";//保存所有灰度发布

}

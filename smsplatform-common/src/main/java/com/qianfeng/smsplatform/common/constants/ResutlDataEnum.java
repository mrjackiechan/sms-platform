package com.qianfeng.smsplatform.common.constants;
//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼            BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  


/**
 * Created by jackiechan on 2021/7/13 15:42
 * 封装了一些不需要携带额外数据的状态数据
 *
 * @author jackiechan
 * 人学始知道，不学非自然。
 */

public enum ResutlDataEnum {
    SUCCESS(InterfaceExceptionDict.RETURN_STATUS_SUCCESS, "成功"),
    CLIENTIDERROR(InterfaceExceptionDict.RETURN_STATUS_CLIENTID_ERROR, "clientId错误"),
    PWDERROR(InterfaceExceptionDict.RETURN_STATUS_PWD_ERROR, "账号密码错误"),
    IPERROR(InterfaceExceptionDict.RETURN_STATUS_IP_ERROR, "访问IP不在白名单"),
    MESSAGECONTENTERROR(InterfaceExceptionDict.RETURN_STATUS_MESSAGE_ERROR, "消息长度不符合要求"),
    MOBILENUMERROR(InterfaceExceptionDict.RETURN_STATUS_MOBILE_ERROR, "手机号错误"),
    SRCIDERROR(InterfaceExceptionDict.RETURN_STATUS_CLIENTID_ERROR, "下行编号错误");

    int code;
    String msg;

    ResutlDataEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}

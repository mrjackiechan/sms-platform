package com.qianfeng.smsplatform.apigateway.filters;

//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼                  BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  
//  


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import com.qianfeng.smsplatform.apigateway.cache.ParamsLocalCache;
import com.qianfeng.smsplatform.common.constants.InterfaceExceptionDict;
import com.qianfeng.smsplatform.common.dto.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Jackiechan on 2021/11/19/10:47
 * 校验请求有没有传递必须传递的公共参数
 *
 * @author Jackiechan
 * @version 1.0
 * @since 1.0
 */
public abstract class BaseParamFilter extends ZuulFilter {
    private ObjectMapper objectMapper;

    @Autowired
    public void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }


    private ParamsLocalCache paramsLocalCache;

    @Autowired
    public void setParamsLocalCache(ParamsLocalCache paramsLocalCache) {
        this.paramsLocalCache = paramsLocalCache;
    }

    @Override
    public String filterType() {
        return FilterConstants.PRE_TYPE;
    }

//    @Override
//    public int filterOrder() {
//        return FilterConstants.PRE_DECORATION_FILTER_ORDER + 2; //当前过滤器要在转发服务之前执行
//    }

    @Override
    public boolean shouldFilter() {
        return RequestContext.getCurrentContext().sendZuulResponse();
    }

    @Override
    public Object run() throws ZuulException {
        //判断有没有传递我们要求的参数,怎么判断?
        RequestContext currentContext = RequestContext.getCurrentContext();
        HttpServletRequest request = currentContext.getRequest();
        //Enumeration<String> parameterNames = request.getParameterNames();//获取到所有的参数名
        // Set set;
        // set.containsAll()//判断集合中的内容是不是都在这个set中,但是问题是只要传递了参数名没有值也会有参数名
        //我们要求的必须传递以为这必须传递具体的值,光判断有没有参数名是没有任何用的,客户可以传递 name=""的方式来规避我们的判断
        //我们可以遍历所有要求传递的参数名,然后挨个从请求中获取数据,如果没有数据或者数据是空的,就代表没有传递这个参数
        List<String> publicParamsList = paramsLocalCache.getParamsMap().get(getParamKey());//根据子类返回的key来决定获取什么参数列表
        if (publicParamsList == null) {
           // currentContext.setSendZuulResponse(false);
           // currentContext.getResponse().setContentType("text/html;charset=utf-8");
          //  currentContext.setResponseBody("参数未传递");
            return null;
        }

        //遍历所有的需要的参数, 从请求中获取数据,将没有传递数据的参数保留下来,放到当前的集合中
        List<String> emptyParamList = publicParamsList.stream().filter(paramName -> {
                    String value = request.getParameter(paramName);//获取到这个参数的值
                    return !StringUtils.hasText(value);//没有内容的数据会保存下来
                }).collect(Collectors.toList());

        if (emptyParamList.size() > 0) {
            //有参数没有传递.拦截请求
            currentContext.setSendZuulResponse(false);
            //告诉用户结果
            //  throw new MyBaseException("以下参数没有传递:" + emptyParamList, InterfaceExceptionDict.PARAMS_VALUE_EMPTY);
            currentContext.addZuulResponseHeader("content-Type", "application/json;charset=utf-8");
            R error = R.error(InterfaceExceptionDict.PARAMS_VALUE_EMPTY, "以下参数没有传递:" + emptyParamList);
            try {
                String resultJson = objectMapper.writeValueAsString(error);
                currentContext.setResponseBody(resultJson);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }

        }

        return null;
    }

    //获取参数列表的key
    public abstract String getParamKey();
}

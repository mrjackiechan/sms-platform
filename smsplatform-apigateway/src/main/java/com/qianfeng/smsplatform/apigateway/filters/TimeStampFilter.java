package com.qianfeng.smsplatform.apigateway.filters;

//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼                  BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  
//  


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import com.qianfeng.smsplatform.common.constants.InterfaceExceptionDict;
import com.qianfeng.smsplatform.common.dto.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;

/**
 * Created by Jackiechan on 2021/11/19/11:34
 *
 * @author Jackiechan
 * @version 1.0
 * @since 1.0
 */
//@Component
public class TimeStampFilter extends ZuulFilter {
    private ObjectMapper objectMapper;

    @Autowired
    public void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }
    @Override
    public String filterType() {
        return FilterConstants.PRE_TYPE;
    }

    @Override
    public int filterOrder() {
        return FilterConstants.PRE_DECORATION_FILTER_ORDER+3;
    }

    @Override
    public boolean shouldFilter() {
        return RequestContext.getCurrentContext().sendZuulResponse();
    }

    @Override
    public Object run() throws ZuulException {
        //判断请求的发起时间和当期的服务器时间的范围差
        RequestContext requestContext = RequestContext.getCurrentContext();
        String timestamp = requestContext.getRequest().getParameter("timestamp");//获取请求的发送时间
        long currentTimeMillis = System.currentTimeMillis();//系统当前时间
        long sendTime = Long.parseLong(timestamp);
        System.err.println(currentTimeMillis);
        if (Math.abs(currentTimeMillis - sendTime) >= 60000) {
            //请求时间和服务器时间相差超过1分钟
            //有参数没有传递.拦截请求
            requestContext.setSendZuulResponse(false);
            //告诉用户结果
            //  throw new MyBaseException("以下参数没有传递:" + emptyParamList, InterfaceExceptionDict.PARAMS_VALUE_EMPTY);
            requestContext.addZuulResponseHeader("content-Type", "application/json;charset=utf-8");
            R error = R.error(InterfaceExceptionDict.TIME_STAMP_ERROR, "请求时间错误");
            try {
                String resultJson = objectMapper.writeValueAsString(error);
                requestContext.setResponseBody(resultJson);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}

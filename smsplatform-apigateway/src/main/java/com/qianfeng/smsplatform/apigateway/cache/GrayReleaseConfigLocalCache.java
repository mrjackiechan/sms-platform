package com.qianfeng.smsplatform.apigateway.cache;

//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼                  BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  
//  


import com.qianfeng.smsplatform.apigateway.event.UpdateApiMappingEvent;
import com.qianfeng.smsplatform.apigateway.feign.CacheService;
import com.qianfeng.smsplatform.apigateway.pojo.GrayReleaseConfig;
import com.qianfeng.smsplatform.common.constants.CacheConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Jackiechan on 2021/11/18/16:56
 *
 * @author Jackiechan
 * @version 1.0
 * @since 1.0
 */
@Component
public class GrayReleaseConfigLocalCache {

    private boolean isLoad;


    private Map<String, List<GrayReleaseConfig>> grayReleaseConfigMap = new HashMap<>();

    public Map<String,  List<GrayReleaseConfig>> getGrayReleaseConfigMap() {
        if (grayReleaseConfigMap.size()==0) {
            init();
        }
        return grayReleaseConfigMap;
    }

    private CacheService cacheService;

    @Autowired
    public void setCacheService(CacheService cacheService) {
        this.cacheService = cacheService;
    }

    public void init() {
        Object object = cacheService.getObject(CacheConstants.CACHE_GRAYRELEASE_KEY);
        System.err.println(object);
        if (object != null&&object instanceof List) {
            grayReleaseConfigMap.clear();
            List<GrayReleaseConfig> releaseConfigList = ((List<Map>) object).stream().map(
                    item -> {
                        GrayReleaseConfig grayReleaseConfig = new GrayReleaseConfig();
                        grayReleaseConfig.setForward((String) item.get("forward"));
                        grayReleaseConfig.setPath((String) item.get("path"));
                        grayReleaseConfig.setServiceId((String) item.get("serviceId"));
                        grayReleaseConfig.setPercent((Integer) item.get("percent"));
                        return grayReleaseConfig;
                    }).collect(Collectors.toList());
//                    .collect(Collectors.toMap(GrayReleaseConfig::getServiceId, releaseConfig -> releaseConfig));
            releaseConfigList.forEach(releaseConfig -> {
                List<GrayReleaseConfig> configList = grayReleaseConfigMap.get(releaseConfig.getServiceId());//先获取看看有没有配置
                if (configList == null) {
                    configList = new ArrayList<>();
                }
                configList.add(releaseConfig);
                grayReleaseConfigMap.put(releaseConfig.getServiceId(), configList);
            });
        }
    }


    @Async
    @EventListener
    public void onEvent(ContextRefreshedEvent event) {
        synchronized (grayReleaseConfigMap) {
            if (!isLoad) {
                isLoad = true;
                init();
            }
        }
    }
    @Async
    @EventListener
    public void onEvent(UpdateApiMappingEvent event) {
       init();
    }

}

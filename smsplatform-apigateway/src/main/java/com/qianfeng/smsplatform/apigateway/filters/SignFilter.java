package com.qianfeng.smsplatform.apigateway.filters;

//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼                  BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  
//  


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import com.qianfeng.smsplatform.apigateway.feign.CacheService;
import com.qianfeng.smsplatform.apigateway.utils.MD5Util;
import com.qianfeng.smsplatform.common.constants.CacheConstants;
import com.qianfeng.smsplatform.common.constants.InterfaceExceptionDict;
import com.qianfeng.smsplatform.common.dto.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Jackiechan on 2021/11/19/16:10
 *
 * @author Jackiechan
 * @version 1.0
 * @since 1.0
 */
@Component
public class SignFilter extends ZuulFilter {
    private ObjectMapper objectMapper;

    @Autowired
    public void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }
    private CacheService cacheService;

    @Autowired
    public void setCacheService(CacheService cacheService) {
        this.cacheService = cacheService;
    }

    @Override
    public String filterType() {
        return FilterConstants.PRE_TYPE;
    }

    @Override
    public int filterOrder() {
        return FilterConstants.PRE_DECORATION_FILTER_ORDER + 3;
    }

    @Override
    public boolean shouldFilter() {
        return RequestContext.getCurrentContext().sendZuulResponse();
    }

    @Override
    public Object run() throws ZuulException {
        //按照规则,将用户传递所有的参数,除了sign参数外的数据进行签名校验,内容为null的数据不参与校验

        RequestContext requestContext = RequestContext.getCurrentContext();
        HttpServletRequest request = requestContext.getRequest();
        //获取到用户传递的所有的参数的集合,注意没有排序
        Map<String, String[]> parameterMap = request.getParameterMap();
        TreeMap treeMap = new TreeMap();
        treeMap.putAll(parameterMap);
        String usercode = request.getParameter("usercode");
        String secure = cacheService.hGet(CacheConstants.CACHE_PREFIX_CLIENT + usercode, "secure");//获取秘钥
        if (secure == null) {
            //没有获取到,说明用户不存在
            //有参数没有传递.拦截请求
            requestContext.setSendZuulResponse(false);
            //告诉用户结果
            //  throw new MyBaseException("以下参数没有传递:" + emptyParamList, InterfaceExceptionDict.PARAMS_VALUE_EMPTY);
            requestContext.addZuulResponseHeader("content-Type", "application/json;charset=utf-8");
            R error = R.error(InterfaceExceptionDict.RETURN_STATUS_CLIENTID_ERROR, "用户不存在");
            try {
                String resultJson = objectMapper.writeValueAsString(error);
                requestContext.setResponseBody(resultJson);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            return null;
        }

        if (!MD5Util.checkSign("utf-8", treeMap,secure)) {
            //有参数没有传递.拦截请求
            requestContext.setSendZuulResponse(false);
            //告诉用户结果
            //  throw new MyBaseException("以下参数没有传递:" + emptyParamList, InterfaceExceptionDict.PARAMS_VALUE_EMPTY);
            requestContext.addZuulResponseHeader("content-Type", "application/json;charset=utf-8");
            R error = R.error(InterfaceExceptionDict.SIGN_ERROR, "签名校验失败");
            try {
                String resultJson = objectMapper.writeValueAsString(error);
                requestContext.setResponseBody(resultJson);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            return null;
        }
        return null;
    }
}

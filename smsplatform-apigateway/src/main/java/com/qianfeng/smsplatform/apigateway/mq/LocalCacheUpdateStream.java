package com.qianfeng.smsplatform.apigateway.mq;
//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼                  BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  
//  


import com.qianfeng.smsplatform.common.constants.RabbitMqConsants;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

/**
 * Created by Jackiechan on 2021/11/18/17:16
 *
 * @author Jackiechan
 * @version 1.0
 * @since 1.0
 */
public interface LocalCacheUpdateStream {
    @Input(RabbitMqConsants.TOPIC_APIMAPPING_UPDATE)// 更新api映射参数规则
    SubscribableChannel updateApiMappingChannel();

    @Input(RabbitMqConsants.TOPIC_PUBLICPARAMS_UPDATE)// 更新公共参数参数规则
    SubscribableChannel updatePublicParamsChannel();


    @Input(RabbitMqConsants.TOPIC_GRAYRELEASE_UPDATE)// 更新灰度发布
    SubscribableChannel updateGrayReleaseChannel();
}

package com.qianfeng.smsplatform.apigateway.cache;

//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼                  BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  
//  


import com.qianfeng.smsplatform.apigateway.event.UpdatePublicParamsEvent;
import com.qianfeng.smsplatform.apigateway.feign.CacheService;
import com.qianfeng.smsplatform.common.constants.CacheConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Jackiechan on 2021/11/19/10:57
 *
 * @author Jackiechan
 * @version 1.0
 * @since 1.0
 */
@Component
public class ParamsLocalCache {

    private boolean isLoad;

    private CacheService cacheService;

    @Autowired
    public void setCacheService(CacheService cacheService) {
        this.cacheService = cacheService;
    }


    private   Map<String, List<String>> paramsMap = new HashMap<>();

    public  Map<String, List<String>> getParamsMap() {
        if (paramsMap.size()==0) {
            init();
        }
        return paramsMap;
    }

    public void init() {
        System.err.println("初始化参数缓存");
        Object object = cacheService.getObject(CacheConstants.CACHE_PUBLICPARAMS_KEY);
        System.err.println(object);
        if (object != null&&object instanceof List) {
            List<String> publicParams = ((List<Map>) object).stream().map(item -> item.get("paramName").toString()).collect(Collectors.toList());
            paramsMap.remove("public");
            paramsMap.put("public", publicParams);

        }
//        List<String> limitinfo = new ArrayList<>();
//        limitinfo.add("page");
//        limitinfo.add("size");
//        paramsMap.put("limits.info", limitinfo);
    }

    @Async
    @EventListener
    public void onEvent(ContextRefreshedEvent event) {
        synchronized (paramsMap) {
            if (!isLoad) {
                isLoad = true;
                init();
            }
        }
    }

    @Async
    @EventListener
    public void onEvent(UpdatePublicParamsEvent event) {
        init();
    }
}

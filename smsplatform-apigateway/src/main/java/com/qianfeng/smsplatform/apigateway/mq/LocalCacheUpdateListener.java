package com.qianfeng.smsplatform.apigateway.mq;

//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼                  BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  
//  


import com.qianfeng.smsplatform.apigateway.event.UpdateApiMappingEvent;
import com.qianfeng.smsplatform.apigateway.event.UpdatePublicParamsEvent;
import com.qianfeng.smsplatform.common.constants.RabbitMqConsants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * Created by Jackiechan on 2021/11/18/17:17
 *
 * @author Jackiechan
 * @version 1.0
 * @since 1.0
 */
@Component
public class LocalCacheUpdateListener {
    private ApplicationContext context;

    @Autowired
    public void setContext(ApplicationContext context) {
        this.context = context;
    }

    @StreamListener(RabbitMqConsants.TOPIC_APIMAPPING_UPDATE)
    public void onApiMappingMessage(String msg) {
        context.publishEvent(new UpdateApiMappingEvent());
    }

    @StreamListener(RabbitMqConsants.TOPIC_PUBLICPARAMS_UPDATE)
    public void onPublicParamsMessage(String msg) {
        context.publishEvent(new UpdatePublicParamsEvent());
    }

    @StreamListener(RabbitMqConsants.TOPIC_GRAYRELEASE_UPDATE)
    public void onGrayReleaseMessage(String msg) {
        context.publishEvent(new UpdatePublicParamsEvent());
    }
}

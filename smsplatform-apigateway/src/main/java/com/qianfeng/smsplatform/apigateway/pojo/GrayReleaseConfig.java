package com.qianfeng.smsplatform.apigateway.pojo;

import com.qianfeng.smsplatform.common.check.CheckNulll;

import java.io.Serializable;

public class GrayReleaseConfig implements Serializable, CheckNulll {
    private String serviceId;

    private String path;

    private Integer percent;

    private String forward;

    private static final long serialVersionUID = 1L;

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId == null ? null : serviceId.trim();
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path == null ? null : path.trim();
    }

    public Integer getPercent() {
        return percent;
    }

    public void setPercent(Integer percent) {
        this.percent = percent;
    }

    public String getForward() {
        return forward;
    }

    public void setForward(String forward) {
        this.forward = forward;
    }
}
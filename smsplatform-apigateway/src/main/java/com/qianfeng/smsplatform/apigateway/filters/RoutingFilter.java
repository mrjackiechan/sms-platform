package com.qianfeng.smsplatform.apigateway.filters;

//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼                  BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  
//  


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import com.qianfeng.smsplatform.apigateway.cache.ApiMappingLocalCache;
import com.qianfeng.smsplatform.apigateway.pojo.UrlMappingBean;
import com.qianfeng.smsplatform.common.constants.InterfaceExceptionDict;
import com.qianfeng.smsplatform.common.dto.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.Map;

/**
 * Created by Jackiechan on 2021/11/18/11:13
 *
 * @author Jackiechan
 * @version 1.0
 * @since 1.0
 */
@Component
public class RoutingFilter extends ZuulFilter {

    private ObjectMapper objectMapper;

    @Autowired
    public void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    private ApiMappingLocalCache apiMappingLocalCache;

    @Autowired
    public void setApiMappingLocalCache(ApiMappingLocalCache apiMappingLocalCache) {
        this.apiMappingLocalCache = apiMappingLocalCache;
    }

    @Override
    public String filterType() {
        return FilterConstants.PRE_TYPE;
    }

    @Override
    public int filterOrder() {
        return FilterConstants.PRE_DECORATION_FILTER_ORDER + 100;
    }

    @Override
    public boolean shouldFilter() {
        //不一定开启,万一拦截了
        return RequestContext.getCurrentContext().sendZuulResponse();
    }

    @Override
    public Object run() throws ZuulException {
        //我们的业务

        //根据用户传递的关键key,拿到我们的映射关系的map,我们要去用户的key是通过method参数传递的
        RequestContext requestContext = RequestContext.getCurrentContext();
        HttpServletRequest servletRequest = requestContext.getRequest();
        String method = servletRequest.getParameter("method");//这就是key
        Map<String, UrlMappingBean> urlMapping = apiMappingLocalCache.getUrlMappingBeanMap();//拿到所有的映射关系
        UrlMappingBean urlMappingBean = urlMapping.get(method);
        if (urlMappingBean != null) {
            String url = urlMappingBean.getInsideApiUrl();
            String serviceId = urlMappingBean.getServiceId();
            requestContext.set(FilterConstants.SERVICE_ID_KEY, serviceId);//转到到其他的服务
            Enumeration<String> parameterNames = servletRequest.getParameterNames();//获取到所有的参数名
            //我们要把当前参数名对应的值替换掉与之对应的占位符
            System.err.println("替换前的url===="+url);
            while (parameterNames.hasMoreElements()) {
                String paramName = parameterNames.nextElement();//获取到每个参数名 比如是id
                String value = servletRequest.getParameter(paramName);//获取到当前参数对应的值 比如是1
                //用value将url中的占位符替换掉
                //占位符我们是{+参数名+} //{id} {name}
                url = url.replace("{" + paramName + "}", value);
            }
            System.err.println("替换hou hou hou的url===="+url);
            requestContext.set(FilterConstants.REQUEST_URI_KEY, url);   //强制转换到对应的地址
        } else {
            //没有
            requestContext.setSendZuulResponse(false);
            requestContext.addZuulResponseHeader("content-Type", "application/json;charset=utf-8");
            R error = R.error(InterfaceExceptionDict.SERVICE_NOT_FOUND, "请求服务不存在");
            try {
                String resultJson = objectMapper.writeValueAsString(error);
                requestContext.setResponseBody(resultJson);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }


        return null;
    }
}

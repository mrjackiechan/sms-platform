package com.qianfeng.strategy.cache;

//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼                  BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  
//  


import com.qianfeng.smsplatform.common.constants.CacheConstants;
import com.qianfeng.strategy.events.DirtyChangeEvent;
import com.qianfeng.strategy.feign.CacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by Administrator on 2021/8/8/19:24
 *
 * @author Administrator
 * @version 1.0
 * @since 1.0
 */
@Component
public class DirtyWordLocalCache {

    private CacheService cacheService;

    @Autowired
    public void setCacheService(CacheService cacheService) {
        this.cacheService = cacheService;
    }

    /**
     * 敏感词库
     */
    @SuppressWarnings("rawtypes")
    private Map sensitiveWordMap;

    public Map getSensitiveWordMap() {
        return sensitiveWordMap;
    }


    public void initKeyWord() {
        Set<String> dirtyWords = cacheService.members(CacheConstants.CACHE_DIRTY_KEY);
        System.err.println("DFA方式敏感词数据长度" + (dirtyWords == null ? 0 : dirtyWords.size()));
        if (dirtyWords != null&&dirtyWords.size()>0) {
            //更新到词库
            addSensitiveWordToHashMap(dirtyWords);
        }else {
            initKeyWord();
        }
    }


    /**
     * 添加数据到敏感词词库
     *
     * @param keyWordSet
     */
    private void addSensitiveWordToHashMap(Set<String> keyWordSet) {
        // 初始化敏感词容器，减少扩容操作
        if (sensitiveWordMap != null) {
            sensitiveWordMap.clear();
        } else {
            sensitiveWordMap = new HashMap(keyWordSet.size());
        }
        String key;
        Map nowMap;
        Map<String, String> newWordMap;
        // 迭代keyWordSet
        for (String aKeyWordSet : keyWordSet) {
            // 关键字
            key = aKeyWordSet;
            nowMap = sensitiveWordMap;
            for (int i = 0; i < key.length(); i++) {
                // 转换成char型
                char keyChar = key.charAt(i);
                // 65279是一个空格
                if ((int) keyChar == 65279) {
                    continue;
                }
                // 获取
                Object wordMap = nowMap.get(keyChar);

                if (wordMap != null) {
                    // 如果存在该key，直接赋值
                    nowMap = (Map) wordMap;
                } else {
                    // 不存在则，则构建一个map，同时将isEnd设置为0，因为他不是最后一个
                    newWordMap = new HashMap<>();
                    // 不是最后一个
                    newWordMap.put("isEnd", "0");
                    nowMap.put(keyChar, newWordMap);
                    nowMap = newWordMap;
                }

                if (i == key.length() - 1) {
                    // 最后一个
                    nowMap.put("isEnd", "1");
                }
            }
        }
    }


    @EventListener
    @Async
    public void onEvent(DirtyChangeEvent event) {
        initKeyWord();//重新初始化敏感词
    }

    /**
     * 程序启动之后执行
     * @param event
     */
    @EventListener
    @Async
    public void onEvent(ContextRefreshedEvent event) {
        initKeyWord(); //更新缓存
    }
}

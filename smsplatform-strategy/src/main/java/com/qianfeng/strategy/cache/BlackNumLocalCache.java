package com.qianfeng.strategy.cache;


//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼            BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  


import com.qianfeng.smsplatform.common.constants.CacheConstants;
import com.qianfeng.strategy.events.BlackChangeEvent;
import com.qianfeng.strategy.feign.CacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by jackiechan on 2021/7/15 11:45
 *
 * @author jackiechan
 * 人学始知道，不学非自然。
 */
@Component

public class BlackNumLocalCache {
    //保存有黑名单的 set 集合
    private static Set<String> blackNums = new HashSet<>();

    private CacheService cacheService;
    @Autowired
    public void setCacheService(CacheService cacheService) {
        this.cacheService = cacheService;
    }


    public void initLocalCache() {
        //从redis中查询数据,保存到本地
        Set<String> blackNumsSet = cacheService.members(CacheConstants.CACHE_BLACK_KEY);//从缓存中获取到所有的黑名单
        if (blackNumsSet != null) {
            //更新本地缓存
            blackNums.clear();
            blackNums.addAll(blackNumsSet);
        }else {
            initLocalCache();
        }

    }


    public static boolean isMember(String member) {
        return blackNums.contains(member);//返回当前的set中是否包含这个数据,返回true代表包含,以为是在黑名单中
    }

    /**
     * 收到事件的时候重新从 redis 中获取数据
     * @param event
     */
    @EventListener
    @Async
    public void onEvent(BlackChangeEvent event) {
        initLocalCache(); //更新缓存
    }

    /**
     * 程序启动之后执行
     * @param event
     */
    @EventListener
    @Async
    public void onEvent(ContextRefreshedEvent event) {
        initLocalCache(); //更新缓存
    }

}

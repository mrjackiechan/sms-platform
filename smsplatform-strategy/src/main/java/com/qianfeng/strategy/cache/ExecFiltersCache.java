package com.qianfeng.strategy.cache;


//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼            BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  


import com.qianfeng.smsplatform.common.constants.CacheConstants;
import com.qianfeng.strategy.events.FiltersChangeEvent;
import com.qianfeng.strategy.feign.CacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jackiechan on 2021/7/19 09:20
 * 要启用的过滤器的本地缓存,当前过滤器在redis中是以list方式存放的,所以可以有顺序
 * @author jackiechan
 * 人学始知道，不学非自然。
 */
@Component
public class ExecFiltersCache {

    private ArrayList<String> filters = new ArrayList<>();

    public ArrayList<String> getFilters() {
        if (filters.size() == 0) {
            init();
        }
        return filters;
    }


    private CacheService cacheService;

    @Autowired
    public void setCacheService(CacheService cacheService) {
        this.cacheService = cacheService;
    }


    public void init() {
       // Set<String> members = cacheService.members(CacheConstants.CACHE_FILTER_KEY);
        List<String> filterLists = cacheService.getDataFromList(CacheConstants.CACHE_STRAGETY_FILTER_KEY, 0, - 1);
        if (filterLists != null && filterLists.size() > 0) {
            filters.clear();
            filters.addAll(filterLists);
        }
    }

    @EventListener
    @Async
    public void onEvent(FiltersChangeEvent event) {
        init(); //更新缓存
    }

    /**
     * 程序启动之后执行
     * @param event
     */
    @EventListener
    @Async
    public void onEvent(ContextRefreshedEvent event) {
        init(); //更新缓存
    }
}

package com.qianfeng.strategy.service.impl;


//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼            BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  


import com.qianfeng.smsplatform.common.constants.CacheConstants;
import com.qianfeng.smsplatform.common.constants.RabbitMqConsants;
import com.qianfeng.smsplatform.common.model.Standard_Report;
import com.qianfeng.smsplatform.common.model.Standard_Submit;
import com.qianfeng.strategy.feign.CacheService;
import com.qianfeng.strategy.service.SendReportService;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by jackiechan on 2021/7/19 09:55
 *
 * @author jackiechan
 * 人学始知道，不学非自然。
 */
@Service
public class SendReportServiceImpl implements SendReportService {

    private AmqpTemplate amqpTemplate;

    private CacheService cacheService;

    @Autowired
    public void setCacheService(CacheService cacheService) {
        this.cacheService = cacheService;
    }

    @Autowired
    public void setAmqpTemplate(AmqpTemplate amqpTemplate) {
        this.amqpTemplate = amqpTemplate;
    }

    @Override
    public void sendSmsReport(Standard_Report report) {
        //将数据发送到接口模块,通知用户结果

        Integer usertype = -1;

        try {
            String usertype1 = cacheService.hGet(CacheConstants.CACHE_PREFIX_CLIENT + report.getClientID(), "usertype");
            usertype = Integer.parseInt(usertype1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (2 == usertype) {
            amqpTemplate.convertAndSend(RabbitMqConsants.TOPIC_PUSH_SMS_REPORT, report);
        }


    }

    @Override
    public void sendSmsToLog(Standard_Submit standard_submit) {
        // standard_submit.setReportState(2);
        // standard_submit.set
        amqpTemplate.convertAndSend(RabbitMqConsants.TOPIC_SMS_SEND_LOG, standard_submit);
    }
}

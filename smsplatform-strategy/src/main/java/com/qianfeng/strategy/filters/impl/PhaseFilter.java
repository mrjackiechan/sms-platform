package com.qianfeng.strategy.filters.impl;


//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼            BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  


import com.qianfeng.smsplatform.common.constants.CacheConstants;
import com.qianfeng.smsplatform.common.constants.InterfaceExceptionDict;
import com.qianfeng.smsplatform.common.constants.StrategyConstants;
import com.qianfeng.smsplatform.common.model.Standard_Submit;
import com.qianfeng.strategy.feign.CacheService;
import com.qianfeng.strategy.filters.FilterChain;
import com.qianfeng.strategy.utils.CheckPhone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * Created by jackiechan on 2021/7/20 10:37
 * 进行号段补全的
 * @author jackiechan
 * 人学始知道，不学非自然。
 */
@Component
public class PhaseFilter  implements FilterChain {

    private CacheService cacheService;
    @Autowired
    public void setCacheService(CacheService cacheService) {
        this.cacheService = cacheService;
    }

    @Override
    public boolean dealSms(Standard_Submit standard_submit) {
        //获取到当前的手机号
        String destMobile = standard_submit.getDestMobile();
        //获取前7位
        String shortnum = destMobile.substring(0, 7);
        //根据这7位从缓存中获取到省市的id,格式是 省id&市id
        String procityid = cacheService.get(CacheConstants.CACHE_PREFIX_PHASE + shortnum);
        if (StringUtils.isEmpty(procityid)) {
            //说明这个手机号不在记录中,不行
            //发送状态报告,这个手机号很诡异,能通过我们的正则表达式验证,但是发现又不属于任何地区
            //throw new NullPointerException();
            standard_submit.setReportState(2);
            standard_submit.setErrorCode(InterfaceExceptionDict.RETURN_STATUS_PHASE_ERROR+"");
            standard_submit.setDescription("当前手机号归属地为查询到");
            //TODO 理论上归属地未查询到实际上应该是我们的数据库不够新,理论上应该是我们更新即可,不应该短信的发送
            // 建议应该是对我们本身发起预警

        }else{
            //查到了对应的省市id
            String[] split = procityid.split("&");
            standard_submit.setProvinceId(Integer.parseInt(split[0]));
            standard_submit.setCityId(Integer.parseInt(split[1]));
            //设置运营商
            if (CheckPhone.isChinaMobilePhoneNum(destMobile)) {
                standard_submit.setOperatorId(StrategyConstants.CHINA_MOBILE);
            } else if (CheckPhone.isChinaTelecomPhoneNum(destMobile)) {
                standard_submit.setOperatorId(StrategyConstants.CHINA_TELCOM);
            }else if (CheckPhone.isChinaUnicomPhoneNum(destMobile)) {
                standard_submit.setOperatorId(StrategyConstants.CHINA_UNICOM);
            }

            return true;
        }


        return false;
    }
}

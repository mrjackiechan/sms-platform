package com.qianfeng.strategy.filters.impl;


//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼            BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  


import com.qianfeng.smsplatform.common.constants.CacheConstants;
import com.qianfeng.smsplatform.common.model.Standard_Submit;
import com.qianfeng.strategy.feign.CacheService;
import com.qianfeng.strategy.filters.FilterChain;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by jackiechan on 2021/7/15 11:38
 *
 * @author jackiechan
 * 人学始知道，不学非自然。
 */
//@Component
public class BlackFilterWithRedisSet implements FilterChain {

    private CacheService cacheService;

    @Autowired
    public void setCacheService(CacheService cacheService) {
        this.cacheService = cacheService;
    }

    @Override
    public boolean dealSms(Standard_Submit standard_submit) {
        //当前的过滤器进行的操作是判断手机号是不是黑名单
        //黑名单我们通过set的方式保存在了redis中
        //要判断的方式非常简单.直接判单当前的手机号在不在set中就可以
        String destMobile = standard_submit.getDestMobile();//获取到当前的手机号
        Boolean isMember = cacheService.isMember(CacheConstants.CACHE_BLACK_KEY, destMobile);
        //如果返回true 意味着是黑名单,否则就不是
        if (isMember) {
            System.err.println(destMobile + "------->在黑名单中set");
        }else{
            System.err.println(destMobile + "========>不不不不不不不在黑名单中set");
            return true;
        }
        return false;
    }
}

package com.qianfeng.strategy.filters.impl;


//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼            BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  


import com.qianfeng.smsplatform.common.constants.CacheConstants;
import com.qianfeng.smsplatform.common.model.Standard_Submit;
import com.qianfeng.strategy.feign.CacheService;
import com.qianfeng.strategy.filters.FilterChain;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

/**
 * Created by jackiechan on 2021/7/15 10:31
 * 当前的过滤器主要负责黑名单处理,主要通过 BLACK:手机号作为key的方式查询
 * @author jackiechan
 * 人学始知道，不学非自然。
 */
//@Component
public class BlackFilter implements FilterChain {


    private CacheService cacheService;

    @Autowired
    public void setCacheService(CacheService cacheService) {
        this.cacheService = cacheService;
    }

    @Override
    public boolean dealSms(Standard_Submit standard_submit) {
        String mobile = standard_submit.getDestMobile();//获取到要发送短信的手机号
        //判断是不是在黑名单中,通过判断这个手机号在redis中有没有对应的数据即可
        String result = cacheService.get(CacheConstants.CACHE_PREFIX_BLACK + mobile);
        if (StringUtils.isEmpty(result)) {
            //不在黑名单
            System.err.println(mobile + "不在黑名单中");
            return true;
        }else{
            //后续要通知客户 当前手机号在黑名单中
            System.err.println(mobile + "----->在黑名单中");
            return false;
        }

    }
}

package com.qianfeng.strategy.filters.impl;


//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼            BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  


import com.qianfeng.smsplatform.common.constants.CacheConstants;
import com.qianfeng.smsplatform.common.constants.InterfaceExceptionDict;
import com.qianfeng.smsplatform.common.model.Standard_Submit;
import com.qianfeng.strategy.feign.CacheService;
import com.qianfeng.strategy.filters.FilterChain;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * Created by jackiechan on 2021/7/21 09:41
 *
 * @author jackiechan
 * 人学始知道，不学非自然。
 */
@Component
public class FeeFilter  implements FilterChain {

    private CacheService cacheService;

    @Autowired
    public void setCacheService(CacheService cacheService) {
        this.cacheService = cacheService;
    }

    @Override
    public boolean dealSms(Standard_Submit standard_submit) {
        //计费用的过滤器, 主要看看用户有没有钱
        //每次发送短信都需要费用,比如一条短信多少钱, 客户需要支付相应的费用,如果钱不够,不让发送
        //先获取到当前客户的每条短信的钱数
        //数据在缓存中存放, 以ROUTE:clientid的方式保存
        String priceString = (String) cacheService.hGet(CacheConstants.CACHE_PREFIX_ROUTER + standard_submit.getClientID(), "price");
        Integer price = 0;
        if (StringUtils.isEmpty(priceString)) {
            //查询出错或者是数据有问题
        }else{
            //判断剩余的钱数是不是够当前短信,通过给用户减指定的金额,剩余的钱数大于等于0代表本次减钱成功
            price = Integer.parseInt(priceString);
            Long money = cacheService.hIncr(CacheConstants.CACHE_PREFIX_CLIENT + standard_submit.getClientID(), "money", 0 - price);

            if (money >= 0) {
                System.err.println("扣钱成功");
                return true;
            }else{
                System.err.println("余额不足");
                standard_submit.setReportState(2);
                standard_submit.setErrorCode(InterfaceExceptionDict.RETURN_STATUS_FEE_ERROR+"");
                standard_submit.setDescription("余额不足");
                cacheService.hIncr(CacheConstants.CACHE_PREFIX_CLIENT + standard_submit.getClientID(), "money", price);
                return false;
            }

        }
        return false;
    }
}

package com.qianfeng.strategy.mq.listeners;


//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼            BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  


import com.qianfeng.smsplatform.common.constants.RabbitMqConsants;
import com.qianfeng.smsplatform.common.model.Standard_Submit;
import com.qianfeng.strategy.service.DataFilterManager;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by jackiechan on 2021/7/14 11:02
 *
 * @author jackiechan
 * 人学始知道，不学非自然。
 */

@Component
public class PreQueueRabbitListener {

    private DataFilterManager dataFilterManager;

    @Autowired
    //@Qualifier("dataFilterManagerImpl2") 这个注解的目是将指定id 的对象注入进来,主要用在这个类型的对象有多个时候
    public void setDataFilterManager(DataFilterManager dataFilterManager) {
        this.dataFilterManager = dataFilterManager;
    }

    /**
     * 收到发送短信的mq后处理消息, 通过具体的实现类来处理消息
     * @param standard_submit
     */
    @RabbitListener(queues = RabbitMqConsants.TOPIC_PRE_SEND)
    public void onMessage(Standard_Submit standard_submit) {
       try {
           System.err.println(standard_submit);
           long currentTimeMillis = System.currentTimeMillis();
           System.err.println(currentTimeMillis);
           //处理消息, 怎么处理? 通过每个策略进行处理,有多少个策略? 不知道,很多个
           dataFilterManager.dealSms(standard_submit);
           System.err.println(System.currentTimeMillis()-currentTimeMillis);
       }catch (Exception e){
          e.printStackTrace();
          //todo 处理异常即可
       }
    }
}

package com.qianfeng.test;


//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼            BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  


import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by jackiechan on 2021/7/16 09:17
 *
 * @author jackiechan
 * 人学始知道，不学非自然。
 */

public class TestMain {
    public static void main(String[] args) {
        String message="朋友你好,你是否有空闲时间想多赚点钱,我们这里有刷单业务,不需要投入什么成本洗钱,只需要有一台可以上网的手机或者电脑即可,另外我们还提供了无抵押贷款,利息低,不查征信,不上征信";
        Set<String> minganci = new HashSet<>();
        minganci.add("刷单");
        minganci.add("高利息");
        minganci.add("低利率");
        minganci.add("诈骗");
        minganci.add("洗钱");
        minganci.add("境外");

        for (String s : minganci) {
            System.err.println(message.contains(s)+"--->"+s);
        }

        List<String> list = minganci.stream().filter(s -> {
            return message.contains(s);
        }).collect(Collectors.toList());

        System.err.println(list);

    }

}

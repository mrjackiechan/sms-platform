package com.qainfeng.smsplatform.api.service.impl;


//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼            BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  


import com.qainfeng.smsplatform.api.feign.CacheService;
import com.qainfeng.smsplatform.api.service.SendReportService;
import com.qianfeng.smsplatform.common.constants.CacheConstants;
import com.qianfeng.smsplatform.common.model.Standard_Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

/**
 * Created by jackiechan on 2021/7/19 10:49
 *
 * @author jackiechan
 * 人学始知道，不学非自然。
 */
@Service
public class SendReportServiceImpl  implements SendReportService {

    private RestTemplate restTemplate;

    private CacheService cacheService;

    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Autowired
    public void setCacheService(CacheService cacheService) {
        this.cacheService = cacheService;
    }

    @Override
    public void sendReport(Standard_Report report) {
        //通知客户
        //通知客户需要知道客户的url
        // 首先获取客户的信息,根据客户的id, 在report中有一个客户id
        Map clientInfoMap = cacheService.hmget(CacheConstants.CACHE_PREFIX_CLIENT + report.getClientID());

        if (clientInfoMap != null && clientInfoMap.size() > 0) {
            //客户用于接收请求的url,我们需要请求这个地址,把数据传过去
            String receivestatusurl = (String) clientInfoMap.get("receivestatusurl");
            //我们约定我们会通过post请求的方式将参数传递过去,并且要求对方返回一个 ok字符串,返回其他任何内容我们都认为是失败
            String result = restTemplate.postForObject(receivestatusurl, report, String.class);
            System.err.println("这是客户返回的结果:===>"+result);
            if (!"ok".equals(result)) {
                report.setSendCount(2);
                //TODO 将当前的状态报告保存起来, 后面通过定时任务进行发送
            }
        }

    }
}

package com.qainfeng.smsplatform.api.controller;


//
//                            _ooOoo_  
//                           o8888888o  
//                           88" . "88  
//                           (| -_- |)  
//                            O\ = /O  
//                        ____/`---'\____  
//                      .   ' \\| |// `.  
//                       / \\||| : |||// \  
//                     / _||||| -:- |||||- \  
//                       | | \\\ - /// | |  
//                     | \_| ''\---/'' | |  
//                      \ .-\__ `-` ___/-. /  
//                   ___`. .' /--.--\ `. . __  
//                ."" '< `.___\_<|>_/___.' >'"".  
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |  
//                 \ \ `-. \_ __\ /__ _/ .-` / /  
//         ======`-.____`-.___\_____/___.-`____.-'======  
//                            `=---='  
//  
//         .............................................  
//                  佛祖镇楼            BUG辟易  
//          佛曰:  
//                  写字楼里写字间，写字间里程序员；  
//                  程序人员写程序，又拿程序换酒钱。  
//                  酒醒只在网上坐，酒醉还来网下眠；  
//                  酒醉酒醒日复日，网上网下年复年。  
//                  但愿老死电脑间，不愿鞠躬老板前；  
//                  奔驰宝马贵者趣，公交自行程序员。  
//                  别人笑我忒疯癫，我笑自己命太贱；  


import com.qainfeng.smsplatform.api.service.ISmsCheckService;
import com.qianfeng.smsplatform.common.constants.ResutlDataEnum;
import com.qianfeng.smsplatform.common.dto.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by jackiechan on 2021/7/13 15:33
 * 我们的目的是接收用户的请求,进行校验后发送短信,最终返回结果给客户
 * 如果检验失败,直接返回结果,这是请求的响应
 * 但是我们后面的发送功能是异步的,所以我们需要在有结果的时候返回结果给客户
 * 问题: 客户可能通过我们发送很多条短信,甚至给某个手机号都发送了很多条短信,如果失败了某条或者某几条,我们如何区分到底哪些失败了
 * 我们需要对每一次的短信进行唯一性的区分,这个区分的方式客户必须知道,比如我们要求客户传递一个唯一的标识过来,然后我们在有结果的时候再把标识传回去
 * @author jackiechan
 * 人学始知道，不学非自然。
 */

@RestController
@RequestMapping("/api")
public class ApiController {

    private ISmsCheckService checkService;

    @Autowired
    public void setCheckService(ISmsCheckService checkService) {
        this.checkService = checkService;
    }

    @PostMapping("/smsinterface")
    public R sendMessage(String srcId, String mobile, String content, String pwd, String clientId, HttpServletRequest request) {

        String realIp = getRealIp(request);//获取到当前请求的ip
        //检查当前要发送的短息是不是符合基本的审核
        List<String> list = checkService.checkSms(clientId, pwd, srcId, realIp, content, mobile);
        if (list == null || list.size() == 0) {
            return R.getR(ResutlDataEnum.SUCCESS);
        }
        R error = R.getR(ResutlDataEnum.MOBILENUMERROR);
        error.put("data", list);//将错误的手机号返回
       // R.getR(ResutlDataEnum.MOBILENUMERROR, list);//将错误的手机号返回
        return error;
    }

    /**
     * 获取当前请求的真实的ip
     * @param req
     * @return
     */
    public static String getRealIp(HttpServletRequest req) {
        String ip = null;
        //获取请求头中标记的真正的ip
        String xIp = req.getHeader("X-Real-IP");
        //我们可能会有多次的反向代理
        String xFor = req.getHeader("X-Forwarded-For");
        //这个 ip 不为空且不是 unknown
        if (!StringUtils.isEmpty(xFor) && !"unknow".equalsIgnoreCase(xFor)) {
            int index = xFor.indexOf(",");//获取第一个,的位置
            if (index > 0) {
                return xFor.substring(0, index);
            }
            return xFor;
        }

        ip = xIp;//有可能这个 ip 也没有

        //当前 ip 是空的
        if (StringUtils.isEmpty(ip) || "unknow".equalsIgnoreCase(ip)) {
            ip = req.getHeader("Proxy-Client-IP");
        }
        //上面获取的有可能还是空的
        if (StringUtils.isEmpty(ip) || "unknow".equalsIgnoreCase(ip)) {
            ip = req.getHeader("WL-Proxy-Client-IP");
        }

        if (StringUtils.isEmpty(ip) || "unknow".equalsIgnoreCase(ip)) {
            ip = req.getHeader("HTTP_CLIENT_IP");
        }

        if (StringUtils.isEmpty(ip) || "unknow".equalsIgnoreCase(ip)) {
            ip = req.getHeader("HTTP_X_FORWARDED_FOR");
        }

        if (StringUtils.isEmpty(ip) || "unknow".equalsIgnoreCase(ip)) {
            ip = req.getRemoteAddr();
        }

        return ip;

    }
}
